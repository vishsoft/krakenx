# KRAKENX

The KRAKENX software calculates a large variety of molecular descriptors based on quantum chemistry computations. The program supports over 2000 three-dimensional descriptors that are calculated from the output of different quantum chemistry packages. The current implementation is geared towards semi-empirical `MOPAC` calculations.

If the software is used, kindly cite:

KRAKENX: software for the generation of alignment-independent 3D descriptors. Venkatraman V and Alsberg BK. *J Mol Model.* 22(4), 93, 2016. [doi:10.1007/s00894-016-2957-5](http://doi.org/10.1007/s00894-016-2957-5)

## Compilation
Ensure that you have Java installed on your machine. Run the `build.sh` script inside the build folder. This should create a `KrakenX.jar` file.

## Usage
The `example`directory contains scripts `getdesc.sh` and `caldesc.sh` to help with the creation of the molecular descriptors. In all cases, molecules must be in SDF format containing 3D-coordinates and each molecule must have a corresponding `MOPAC` output file. `MOPAC` is typically run with the following keywords in the input file:
`PM6 PRECISE MMOK XYZ ENPART STATIC SUPER BONDS LARGE PRTXYZ DISP ALLVEC`. 

We will assume that the molecule SDF files are in the `mols` directory and the corresponding `MOPAC` output files in the directory `mopdata`. To calculate descriptors, the user can use/modify the `caldesc.sh` script which requires 2 inputs: the location of the `mols` directory and the directory `mopdata`. The choice of which descriptors are to be calculated can be made by setting the descriptor name to Y/N in the `caldesc.sh` script. 

`./caldesc.sh mols mopdata [prefix]`

The prefix is the text that is prepended to the descriptor names. It is typically useful when calculating descriptors for cations and anions of an ionic liquid independently.   

## Types of descriptors
KrakenX mainly focuses on 3D and topographical descriptors (topological descriptors with 3D features included). Using the software, the following set of descriptors can be either derived or directly extracted from the MOPAC output files:

1. **Quantum chemical**: Quantities such as the heat of formation, area, volume, HOMO, LUMO, HOMO-LUMO gap, dipole moment, electronic energy, total self polarizability, electron-nuclear attraction and repulsion energies, total electrophilic and nucleophilic delocalizabilities, principal moments of inertia, polarizability and hyperpolarizability, the vibrational, rotational and translational values for the enthalpy, enthalpy and heat capacity.
2. **Atomic charge-based**: Minimum and maximum atomic partial charge, polarity parameters, partial charge-weighted topological electronic descriptor, local dipole index, total squared charge, total absolute charge, total positive and negative charge.
3. **CPSA**: Charge partial surface area descriptors that combine partial charges and atomic solvent accessible surface areas (solvent radius set to 1.4$`\overset{\circ}{A}`$ ).
4. **EVA/EEVA**: A density distribution-based (pseudo-spectral descriptors) eigenvalue (EVA) and electronic eigenvalue (EEVA) descriptors that are derived from the vibrational frequencies and molecular orbital energies of a geometry-optimized structure.
5. **Shape and geometry**: This class of descriptors includes 3D Wiener index, inertial shape factor, radius of gyration, ovality, molecular eccentricity, asphericity, globularity, radial distribution function (RDF), 3D Molecule Representation of Structure based on Electron diffraction (MoRSE), Weighted Holistic Invariant Molecular (WHIM).
6. **Topographical**: 3D topological distance-based autocorrelation descriptors can be calculated by considering the average Euclidean distance between all atoms located at a given topological distance $`d`$. 3D BCUT values can be calculated by encoding 3D atomic properties (charge, self-polarizability etc.) on the diagonals of the molecular connection table with off-diagonals encoding interatomic distances.
7. **Coulomb Matrix**: A global descriptor which mimics the electrostatic interaction between nuclei.
8. **Graph energy**: The ordinary energy of a graph is defined as the sum of the absolute values of the eigenvalues of its adjacency matrix. Here, ```KrakenX ``` calculates an atomic property weighted equivalent.


For each atom, the charge, electrophilic and nucleophilic frontier electron density, the electrophilic, nucleophilic and radical superdelocalizability, and the atom selfpolarizability are used as atom-level weights in the calculation of descriptors such as the RDF, MoRSE, WHIM, and autocorrelation vectors. The atom-centered charges can be set to either the default MOPAC derived values, electrostatic potential (ESP), or user-defined charges (UDF) such as those based on the electronegativity equalization method.



