package krakenx;

/**
 * Functions for reading and storing properties obtained from MOPAC output files
 * @author Vishwesh Venkatraman
 */
import Jama.Matrix;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.vecmath.Point3d;

/**
 * Utility to read MOPAC output
 *
 */
public class MOPACReader
{
    private int status;
    private final boolean debug;
    private String errorString;

    private double heatOfFormation = 0; // KCal/mol
    private double totalEnergy = 0; // eV
    private double electronicEnergy = 0; // eV
    private double coreCoreRepulsion = 0; // eV
    private double cosmoArea = 0; // square angstroms
    private double cosmoVolume = 0; // cubic angstroms
    private double HOMOEnergy = 0; // eV
    private double LUMOEnergy = 0; // eV
    private double ionizationPotential = 0; // eV
    private double totalDipoleMoment = 0; // debye units
    private double dipolePointCharge = 0;
    private double dipoleHybrid = 0;
    private double resonanceEnergy = 0;
    private double exchangeEnergy = 0; // exchange correlation energy
    private double electronElectronRepulsion = 0;
    private double electronNuclearAttraction = 0;
    private double totalElectrostaticInteraction = 0;
    private final double mullikenElectronegativity = 0;
    private double absoluteHardness = 0;
    private double totalNucleophilicDelocalizability = 0;
    private double totalElectrophilicDelocalizability = 0;
    private double totalSelfPolarizability = 0;
    private double averagePolarizability = 0;
    private final double hbcontrib = 0;
    private int numFilledLevels = 0;
    private String pointGrp;


    private double molecularWeight = 0;
    private double PIMX = 0; // principal moments of inertia
    private double PIMY = 0;
    private double PIMZ = 0;

    private final ArrayList<Double> polar_desc;

    private final ArrayList<Double> atom_selfpol;
    private final ArrayList<Double> atom_chgden;
    private final ArrayList<Double> atom_dnr;
    private final ArrayList<Double> atom_der;
    private final ArrayList<String> atom_symbols;
    private final ArrayList<Double> atom_charges;
    private final ArrayList<Point3d> atom_coords;
    private final ArrayList<Double> moEnergies;
    private final ArrayList<Double> vibFreq;
    private final ArrayList<Double> mo_bndctrb;
    private final ArrayList<Double> esp_charges;
    private final ArrayList<Double> tdip;
    private final ArrayList<AtomicOrbital> lstOrb;
    private final ArrayList<Double> atom_efed;
    private final ArrayList<Double> atom_nfed;
    private final ArrayList<Double> alpha_polar;
    private final ArrayList<ThermVals> thermo_energies;
    private Matrix bndMatrix = null;




//------------------------------------------------------------------------------

    public MOPACReader()
    {
        moEnergies = new ArrayList<>();
        vibFreq = new ArrayList<>();
        atom_symbols = new ArrayList<>();
        status = -1;
        debug = false;
        atom_charges = new ArrayList<>();
        atom_coords = new ArrayList<>();
        atom_selfpol = new ArrayList<>();
        atom_chgden = new ArrayList<>();
        atom_dnr = new ArrayList<>();
        atom_der = new ArrayList<>();
        mo_bndctrb = new ArrayList<>();
        polar_desc = new ArrayList<>();
        errorString = "See MOPAC output for details.";
        esp_charges = new ArrayList<>();
        tdip = new ArrayList<>();
        lstOrb = new ArrayList<>();
        atom_efed = new ArrayList<>();
        atom_nfed = new ArrayList<>();
        alpha_polar = new ArrayList<>();
        thermo_energies = new ArrayList<>();
    }

//------------------------------------------------------------------------------

    /**
     * Parse MOPAC output data
     * @param infile
     * @throws Exception 
     */
    
    public void readMOPACData(String infile) throws Exception
    {
        if (debug)
        {
            System.out.println("Reading file " + infile);
        }

        String[] vals;
        String str, line;

        String EMSG = "Error and normal termination messages";
        String NACADC = "NET ATOMIC CHARGES AND DIPOLE CONTRIBUTIONS";
        String NOPARS = "Parameters for some elements are missing";;
        int foundCart = 0, foundMOE = 0, foundPIM = 0, foundBO = 0, foundEigen = 0;


        BufferedReader br = null;

        boolean foundVib = false, fndHOF = false;
        int foundXYZ = 0, foundPol = 0, foundDelocalizabilities = 0, foundTherm = 0;
        int foundChargeData = 0, foundChgDen = 0, foundSelfPol = 0;
        int foundESP = 0, foundAlphaPol = 0, foundStatPol = 0;
        
        ArrayList<String> thermlines = new ArrayList<>();


        try
        {
            br = new BufferedReader(new FileReader(infile));
            while ((line = br.readLine()) != null)
            {
                if ((line.trim()).length() == 0)
                {
                    continue;
                }
                
                if (line.startsWith(" ****"))
                {
                    if (foundStatPol > 0)
                        foundStatPol = 0;
                }
                
                if (line.trim().toUpperCase().contains(EMSG.toUpperCase()))
                {
                    errorString = EMSG;
                    status = -1;
                    break;
                }

                if (line.trim().toUpperCase().contains(NOPARS.toUpperCase()))
                {
                    errorString = NOPARS;
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("ODD NUMBER OF ELECTRONS, CORRECT FAULT"))
                {
                    errorString = "SYSTEM SPECIFIED WITH ODD NUMBER OF ELECTRONS, CORRECT FAULT";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("EXCESS NUMBER OF OPTIMIZATION CYCLES"))
                {
                    errorString = "EXCESS NUMBER OF OPTIMIZATION CYCLES";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("NUMBER OF CYCLES EXCEEDED"))
                {
                    errorString = "NUMBER OF CYCLES EXCEEDED";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("CONTAINS PEPTIDE LINKAGES"))
                {
                    errorString = "THE SYSTEM CONTAINS PEPTIDE LINKAGES";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("CALCULATION ABANDONED AT THIS POINT"))
                {
                    errorString = "CALCULATION ABANDONED AT THIS POINT";
                    status = -1;
                    break;
                }

                if (line.contains("NOT ENOUGH TIME FOR ANOTHER CYCLE"))
                {
                    errorString = "GEOMETRY OPTIMIZATION FAILED";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("THE NUMBER OF POINTS"))
                {
                    foundESP = -1;
                    continue;
                }

                if (line.contains("ELECTROSTATIC POTENTIAL CHARGES"))
                {
                    foundESP = 1;
                    continue;
                }


                if (line.toUpperCase().contains("FAILED TO ACHIEVE SCF"))
                {
                    errorString = "FAILED TO ACHIEVE SCF";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("GRADIENT IS TOO LARGE"))
                {
                    errorString = "GRADIENT IS TOO LARGE TO ALLOW FORCE MATRIX TO BE CALCULATED, (LIMIT=10)";
                    status = -1;
                    break;
                }

                if (line.toUpperCase().contains("SCF FIELD WAS ACHIEVED"))
                {
                    status = 0;
                    continue;
                }
                
                if (line.startsWith("MOLECULAR POINT GROUP"))
                {
                    vals = line.split("\\s+");
                    pointGrp = vals[vals.length-1];
                    break;
                }


                if (line.trim().equalsIgnoreCase("EIGENVECTORS"))
                {
                    // read block
                    br.readLine();
                    this.readEigenBlock(br);
                    foundEigen = 1;
                    foundDelocalizabilities = 1;
                    foundMOE = -1;
                    continue;
                }


                if (line.trim().contains("TOTAL ENERGY PARTITIONING"))
                    continue;


                if (line.trim().contains("BONDING CONTRIBUTION OF EACH M.O."))
                {
                    foundBO = 1;
                    continue;
                }

                if (line.trim().contains("BOND ORDERS"))
                {
                    foundBO = -1;
                    bndMatrix = new Matrix(atom_symbols.size(), atom_symbols.size());
                    br.readLine();
                    this.readBondValenceBlock(br, bndMatrix);
                    continue;
                }

                if (line.trim().startsWith("NO. OF FILLED LEVELS"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    numFilledLevels = Integer.parseInt(vals[0]);
                    //System.out.println("NO. OF FILLED LEVELS: " + numFilledLevels);
                    continue;
                }
                
                
                if (line.trim().contains("Average Polarizability from"))
                {
                    foundStatPol = 1;
                    continue;
                }


                if (line.trim().contains("CALCULATED THERMODYNAMIC PROPERTIES"))
                {
                    foundTherm = 1;
                    continue;
                }

                if (foundBO == 1)
                {
                    vals = line.trim().split("\\s+");
                    for (int i=0; i<vals.length; i++)
                    {
                        mo_bndctrb.add(Double.parseDouble(vals[i]));
                    }
                }
                
                if (foundStatPol == 1)
                {
                    if (line.trim().contains("A.U."))
                    {
                        vals = line.trim().split("\\s+");
                        this.averagePolarizability = Double.parseDouble(vals[0]);
                    }
                }


                if (foundESP == 1)
                {
                    if (line.contains("ATOM NO.") && line.contains("TYPE") &&
                        line.contains("CHARGE"))
                    {
                        foundESP = 2;
                        continue;
                    }
                }

                if (foundESP == 2)
                {
                    vals = line.trim().split("\\s+");
                    esp_charges.add(new Double(vals[2]));
                }

                if (foundTherm == 1)
                {
                    
                    if (line.trim().contains("VIB.")) 
                    {
                        thermlines.add(line.trim());
                    }
                    
                    if (line.trim().contains("ROT.")) 
                    {
                        thermlines.add(line.trim());
                    }
                    
                    if (line.trim().contains("INT.")) 
                    {
                        thermlines.add(line.trim());
                    }
                    
                    if (line.trim().contains("TRA.")) 
                    {
                        thermlines.add(line.trim());
                    }
                    
                    if (line.trim().contains("TOT.")) 
                    {
                        thermlines.add(line.trim());
                        ThermVals tvals = new ThermVals();;
                        for (int l=0; l<thermlines.size(); l++)
                        {
                            vals = thermlines.get(l).split("\\s+");
                            if (l == 0) 
                            {
                                tvals.setTemperature(Double.parseDouble(vals[0]));
                                tvals.setVibEnthalpy(Double.parseDouble(vals[3]));
                                tvals.setVibHeatCap(Double.parseDouble(vals[4]));
                                tvals.setVibEntropy(Double.parseDouble(vals[5]));
                            }
                            if (l == 1)
                            {
                                tvals.setRotEnthalpy(Double.parseDouble(vals[2]));
                                tvals.setRotHeatCap(Double.parseDouble(vals[3]));
                                tvals.setRotEntropy(Double.parseDouble(vals[4]));
                            }
                            if (l == 2)
                            {
                                tvals.setIntEnthalpy(Double.parseDouble(vals[2]));
                                tvals.setIntHeatCap(Double.parseDouble(vals[3]));
                                tvals.setIntEntropy(Double.parseDouble(vals[4]));
                            }
                            if (l == 3)
                            {
                                tvals.setTraEnthalpy(Double.parseDouble(vals[2]));
                                tvals.setTraHeatCap(Double.parseDouble(vals[3]));
                                tvals.setTraEntropy(Double.parseDouble(vals[4]));
                            }
                            if (l == 4)
                            {
                                tvals.setHeatOfFormation(Double.parseDouble(vals[1]));
                                tvals.setTotEnthalpy(Double.parseDouble(vals[2]));
                                tvals.setTotHeatCap(Double.parseDouble(vals[3]));
                                tvals.setTotEntropy(Double.parseDouble(vals[4]));
                            }
                        }
                        this.thermo_energies.add(tvals);
                        thermlines.clear();                        
                        continue;
                    }
                    
//                    if (line.trim().startsWith("298.00  VIB."))
//                    {
//                        vals = line.trim().split("\\s+");
//                        this.thermo_desc.add(Double.parseDouble(vals[3]));
//                        this.thermo_desc.add(Double.parseDouble(vals[4]));
//                        this.thermo_desc.add(Double.parseDouble(vals[5]));
//                    }
//
//                    if (line.trim().startsWith("ROT."))
//                    {
//                        vals = line.trim().split("\\s+");
//                        this.thermo_desc.add(Double.parseDouble(vals[2]));
//                        this.thermo_desc.add(Double.parseDouble(vals[3]));
//                        this.thermo_desc.add(Double.parseDouble(vals[4]));
//                    }
//
//
//                    if (line.trim().startsWith("TRA."))
//                    {
//                        vals = line.trim().split("\\s+");
//                        this.thermo_desc.add(Double.parseDouble(vals[2]));
//                        this.thermo_desc.add(Double.parseDouble(vals[3]));
//                        this.thermo_desc.add(Double.parseDouble(vals[4]));
//                    }
//
//                    if (line.trim().startsWith("TOT."))
//                    {
//                        foundTherm = -1;
//                        continue;
//                    }
                }
                
                if (line.trim().contains("*: NOTE: HEATS OF FORMATION ARE RELATIVE"))
                {
                    foundTherm = -1;
                    continue;
                }

                if (line.trim().contains("Calculation of polarizability failed"))
                {
                    if (polar_desc.isEmpty())
                    {
                        polar_desc.add(0.0);
                        polar_desc.add(0.0);
                        polar_desc.add(0.0);
                    }
                    continue;
                }


                if (line.trim().contains("COMPONENTS OF ALPHA"))
                {
                    //System.err.println("HERE ....");
                    foundAlphaPol += 1;
                    continue;
                }

                if (foundAlphaPol < 2)
                {
                    //System.err.println("alpha_pol" + alpha_polar.toString());
                    //Utils.pause();

                    if (line.contains("*X           *Y           *Z"))
                        continue;
                    if (alpha_polar.size() <= 9)
                    {    
                        if (line.trim().startsWith("*=X:"))
                        {
                            vals = line.trim().split("\\s+");
                            for (int j=1; j<vals.length; j++)
                               this.alpha_polar.add(Double.parseDouble(vals[j]));
                        }
                        if (line.trim().startsWith("*=Y:"))
                        {
                            vals = line.trim().split("\\s+");
                            for (int j=1; j<vals.length; j++)
                               this.alpha_polar.add(Double.parseDouble(vals[j]));
                        }
                        if (line.trim().startsWith("*=Z:"))
                        {
                            vals = line.trim().split("\\s+");
                            for (int j=1; j<vals.length; j++)
                               this.alpha_polar.add(Double.parseDouble(vals[j]));
                        }
                    }
                }

                if (line.trim().startsWith("ISOTROPIC AVERAGE ALPHA"))
                {
                    foundPol += 1;
                    if (foundPol <= 1)
                    {
                        vals = line.trim().split("\\s+");
                        //System.err.println("Adding ALPHA");
                        if (line.contains("*****"))
                            polar_desc.add(0.0);
                        else
                            polar_desc.add(Double.parseDouble(vals[4]));
                    }
                }

                if (line.trim().startsWith("AVERAGE BETA (SHG) VALUE AT   0.00000"))
                {
                    foundPol += 1;
                    vals = line.trim().split("\\s+");
                    //System.err.println("Adding BETA");
                    if (line.contains("*****"))
                        polar_desc.add(0.0);
                    else
                        polar_desc.add(Double.parseDouble(vals[8]));
                }


                if (line.trim().startsWith("AVERAGE GAMMA VALUE AT    0.00000"))
                {
                    foundPol += 1;
                    vals = line.trim().split("\\s+");
                    //System.err.println("Adding GAMMA");
                    if (line.contains("*****"))
                        polar_desc.add(0.0);
                    else
                        polar_desc.add(Double.parseDouble(vals[6]));
                }


                if (line.contains("NUMBER") && line.contains("SYMBOL") &&
                        line.contains("ANGSTROMS"))
                {
                    if (atom_coords.isEmpty())
                        foundXYZ = 1;
                    status = 0;
                    continue;
                }

                if (line.contains("PRINCIPAL MOMENTS OF INERTIA"))
                {
                    //System.err.println("PRINCIPAL MOMENTS OF INERTIA");
                    foundPIM = 1;
                    continue;
                }

                if (foundPIM == 1)
                {
                    if (line.trim().startsWith("A ="))
                    {
                        vals = line.trim().split("\\s+");
                        PIMX = Double.parseDouble(vals[2]);
                        PIMY = Double.parseDouble(vals[5]);
                        PIMZ = Double.parseDouble(vals[8]);
                        foundPIM = 0;
                        continue;
                    }
                }

                if (line.startsWith(" POINT-CHG."))
                {
                    vals = line.trim().split("\\s+");
                    dipolePointCharge = Double.parseDouble(vals[4]);
                    continue;
                }

                if (line.startsWith(" HYBRID"))
                {
                    vals = line.trim().split("\\s+");
                    dipoleHybrid = Double.parseDouble(vals[4]);
                    continue;
                }


                if (line.startsWith(" SUM"))
                {
                    vals = line.trim().split("\\s+");
                    totalDipoleMoment = Double.parseDouble(vals[4]);
                    continue;
                }

                if (line.contains("FINAL HEAT OF FORMATION"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    heatOfFormation = Double.parseDouble(vals[0]);
                    //System.out.println("HEAT OF FORMATION: " + heatOfFormation);
                    fndHOF = true;
                    status = 1;
                    continue;
                }

                if (line.trim().startsWith("TOTAL ENERGY"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    totalEnergy = Double.parseDouble(vals[0]);
                    //System.out.println("TOTAL ENERGY: " + totalEnergy);
                    continue;
                }


                if (line.trim().startsWith("ELECTRONIC ENERGY"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    electronicEnergy = Double.parseDouble(vals[0]);
                    //System.out.println("ELECTRONIC ENERGY: " + electronicEnergy);
                    continue;
                }

                if (line.trim().startsWith("CORE-CORE REPULSION"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    coreCoreRepulsion = Double.parseDouble(vals[0]);
                    //System.out.println("CORE-CORE REPULSION: " + coreCoreRepulsion);
                    continue;
                }

                if (line.trim().startsWith("COSMO AREA"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    cosmoArea = Double.parseDouble(vals[0]);
                    //System.out.println("COSMO AREA: " + cosmoArea);
                    continue;
                }

                if (line.trim().startsWith("COSMO VOLUME"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    cosmoVolume = Double.parseDouble(vals[0]);
                    //System.out.println("COSMO VOLUME: " + cosmoVolume);
                    continue;
                }


                if (line.trim().startsWith("IONIZATION POTENTIAL"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    ionizationPotential = Double.parseDouble(vals[0]);
                    //System.out.println("IONIZATION POTENTIAL: " + ionizationPotential);
                    continue;
                }


                if (line.trim().startsWith("MOLECULAR WEIGHT"))
                {
                    str = line.substring(line.indexOf("=") + 1);
                    vals = str.trim().split("\\s+");
                    molecularWeight = Double.parseDouble(vals[0]);
                    //System.out.println("MOLECULAR WEIGHT: " + molecularWeight);
                    continue;
                }


                if (line.trim().contains("Formula"))
                {
                    foundXYZ = 2;
                    continue;
                }


                if (line.trim().startsWith("TOTAL ELECTROSTATIC INTERACTION"))
                {
                    vals = line.trim().split("\\s+");
                    this.totalElectrostaticInteraction = Double.parseDouble(vals[3]);
                    continue;
                }

                if (line.trim().startsWith("EXCHANGE ENERGY"))
                {
                    vals = line.trim().split("\\s+");
                    this.exchangeEnergy = Double.parseDouble(vals[2]);
                    continue;
                }

                if (line.trim().startsWith("RESONANCE ENERGY"))
                {
                    vals = line.trim().split("\\s+");
                    this.resonanceEnergy = Double.parseDouble(vals[2]);
                    continue;
                }


                if (foundXYZ == 1 && fndHOF)
                {
                    if (Character.isDigit(line.trim().charAt(0)))
                    {
                        String[] crds = line.trim().split("\\s+");
                        if (crds.length == 8)
                        {
                            atom_symbols.add(crds[1].trim());
                            Point3d pt = new Point3d(Double.parseDouble(crds[2]),
                                    Double.parseDouble(crds[4]),
                                    Double.parseDouble(crds[6]));
                            atom_coords.add(pt);
                            //System.err.println(crds[1] + " " + pt.x + " " + pt.y + " " + pt.z);
                        }
                        else
                        {
                            //System.err.println("Setting foundXYZ: " + atom_coords.size());
                            foundXYZ = 2;
                        }
                    }
                }

                if (line.trim().equalsIgnoreCase("CARTESIAN COORDINATES"))
                {
                    foundCart++;
                    continue;
                }

                if (line.trim().startsWith("Parr & Pople"))
                {
                    vals = line.trim().split("\\s+");
                    this.absoluteHardness = Double.parseDouble(vals[5]);
                    continue;
                }

                if (line.trim().startsWith("EIGENVALUES"))
                {
                    foundMOE = 1;
                    continue;
                }

                if (line.trim().equalsIgnoreCase(NACADC))
                {
                    foundMOE = -1;
                    continue;
                }

                if (line.trim().equalsIgnoreCase("SUPERDELOCALIZABILITIES"))
                {
                    foundDelocalizabilities = 1;
                    foundMOE = -1;
                    continue;
                }

                if (foundDelocalizabilities == 1 && line.startsWith(" Ehomo"))
                {
                    str = line.substring(line.indexOf(":") + 1).trim();
                    HOMOEnergy = Double.parseDouble(str);
                    continue;
                }

                if (foundDelocalizabilities == 1 && line.startsWith(" Elumo"))
                {
                    str = line.substring(line.indexOf(":") + 1).trim();
                    LUMOEnergy = Double.parseDouble(str);
                    continue;
                }

                if (foundDelocalizabilities == 1 && line.contains("q(r) - Z(r)"))
                {
                    foundChgDen = 1;
                    continue;
                }

                if (foundDelocalizabilities == 1 && line.contains("piS(r)"))
                {
                    foundSelfPol = 1;
                    continue;
                }

                if (foundDelocalizabilities == 1 && line.startsWith(" Total:"))
                {
                    vals = line.trim().split("\\s+");
                    if (vals.length == 3)
                    {
                        foundChgDen = 2;
                        totalNucleophilicDelocalizability = Double.parseDouble(vals[1]);
                        totalElectrophilicDelocalizability = Double.parseDouble(vals[2]);
                    }
                    else if (vals.length == 2)
                    {
                        foundSelfPol = 2;
                        totalSelfPolarizability = Double.parseDouble(vals[1]);
                    }
                }

                if (foundDelocalizabilities == 1 && foundChgDen == 1)
                {
                    vals = line.trim().split("\\s+");
                    if (vals.length == 5)
                    {
                        atom_dnr.add(Double.parseDouble(vals[2]));
                        atom_der.add(Double.parseDouble(vals[3]));
                    }
                }

                if (foundDelocalizabilities == 1 && foundSelfPol == 1)
                {
                    vals = line.trim().split("\\s+");
                    if (vals.length == 3)
                    {
                        atom_selfpol.add(Double.parseDouble(vals[2]));
                    }
                }

                if (foundMOE == 1)
                {
                    vals = line.trim().split("\\s+");
                    //System.err.println("VALS: " + vals.length);
                    for (int i = 0; i < vals.length; i++)
                    {
                        if (vals[i].trim().length() > 0)
                        {
                            moEnergies.add(new Double(vals[i].trim()));
                        }
                    }
                }

                if (line.trim().startsWith("ELECTRON-ELECTRON REPULSION"))
                {
                    vals = line.trim().split("\\s+");
                    this.electronElectronRepulsion = Double.parseDouble(vals[2]);
                    continue;
                }

                if (line.trim().startsWith("ELECTRON-NUCLEAR ATTRACTION"))
                {
                    vals = line.trim().split("\\s+");
                    this.electronNuclearAttraction = Double.parseDouble(vals[2]);
                    continue;
                }

                if (line.contains("No. of ELECS."))
                {
                    foundChargeData = 1;
                    continue;
                }

                if (line.startsWith(" DIPOLE"))
                {
                    foundChargeData = -1;
                    continue;
                }

                String tmp = line.trim();
                if (tmp.startsWith("NO.") && tmp.endsWith("Z") && foundCart == 2)
                {
                    continue;
                }

                if (line.contains("ATOMIC ORBITAL ELECTRON POPULATIONS"))
                {
                    status = 1;
                    continue;
                }

                if (line.trim().contains("DESCRIPTION OF VIBRATIONS"))
                {
                    foundVib = true;
                    continue;
                }

//                if (foundAtomCoords == 1 && atom_coords.isEmpty() && foundXYZ != 2)
//                {
//                    //System.err.println("foundAtomCoords=1");
//                    // read the charges
//                    vals = line.trim().split("\\s+");
//
//                    if (vals.length == 5)
//                    {
//                        atom_symbols.add(vals[1].trim());
//                        //int atomIdx = Integer.parseInt(vals[0]) - 1;
//                        Point3d pt = new Point3d(Double.parseDouble(vals[2]),
//                                Double.parseDouble(vals[3]),
//                                Double.parseDouble(vals[4]));
//                        atom_coords.add(pt);
//                    }
//                }

                if (foundChargeData == 1)
                {
                    //System.err.println("foundChargeData=1");
                    // read the charges
                    vals = line.trim().split("\\s+");
                    //int atomIdx = Integer.parseInt(vals[0]) - 1;
                    double charge = Double.parseDouble(vals[2]);
                    atom_charges.add(charge);
                    atom_chgden.add(Double.parseDouble(vals[3]));
                }

                if (line.trim().startsWith("FREQ."))
                {
                    vals = line.trim().split("\\s+");
                    vibFreq.add(new Double(vals[1]));
                }

                if (line.trim().startsWith("T-DIPOLE."))
                {
                    vals = line.trim().split("\\s+");
                    tdip.add(new Double(vals[1]));
                }
            }
        }
        catch (NumberFormatException | IOException nfe)
        {
            throw nfe;
        }
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ioe)
            {
                throw ioe;
            }
        }

        if (foundVib)
        {
            status = 2;
        }

        if (numFilledLevels > 0)
        {
            HOMOEnergy = moEnergies.get(numFilledLevels-1);
            if (moEnergies.size() <= numFilledLevels)
            {
                LUMOEnergy = 0.0;
            }
            else
                LUMOEnergy = moEnergies.get(numFilledLevels);
             
        }

        // calculate Electrophilic frontier electron density, Nucleophilic
        // frontier electron density
        if (foundEigen == 1)
        {
            for (int i=0; i<atom_charges.size(); i++)
            {
                atom_efed.add(this.getElectrophilicFrontierED(i+1));
                atom_nfed.add(this.getNucleophilicFrontierED(i+1));
            }
        }
    }

//------------------------------------------------------------------------------

    private void readBondValenceBlock(BufferedReader br, Matrix bndMatrix) throws IOException
    {
        String line;
        int flag = -1, anum;
        String[] vals = null;



        while (!((line = br.readLine()).trim().contains("SELF-Q:")))
        {
            if (line.trim().isEmpty())
                continue;

            if (line.trim().contains("SUMMARY OF ENERGY PARTITION"))
                break;

            if ( line.charAt(0) >= 'A' && line.charAt(0) <= 'Z' )
            {
                flag = 1;
                continue;
            }
            if (line.startsWith("--"))
            {
                flag = 2;
                continue;
            }

            if (flag == 2)
            {
                vals = line.trim().split("\\s+");
                anum = Integer.parseInt(vals[1]);
                int k = 0;
                for (int i=2; i<vals.length; i++)
                {
                    bndMatrix.set(anum-1, k, Double.parseDouble(vals[i]));
                    k++;
                }
            }
        }
    }

//------------------------------------------------------------------------------

    private void readEigenBlock(BufferedReader br) throws IOException
    {
        String line, atype, otype;
        int flag = -1, anum, idx;
        String[] vals = null;

        while (!((line = br.readLine()).trim().equals("SUPERDELOCALIZABILITIES")))
        {
            if (line.trim().isEmpty())
                continue;

            if (line.trim().contains("Root No."))
            {
                flag = 1;
                continue;
            }

            if (flag == 1)
            {
                flag = 2;
                continue;
            }

            if (flag == 2)
            {
                vals = line.trim().split("\\s+");
                for (String str:vals)
                {
                    moEnergies.add(Double.parseDouble(str));
                }
                flag = -1;
                continue;
            }

            if (flag == -1)
            {
                vals = line.trim().split("\\s+");

                anum = Integer.parseInt(vals[2]);
                atype = vals[1];
                otype = vals[0];

                idx = this.getIndex(anum, atype, otype);
                if (idx == -1)
                {
                    ArrayList<Double> coeffs = new ArrayList<>();
                    for (int i=3; i<vals.length; i++)
                    {
                        coeffs.add(Double.parseDouble(vals[i]));
                    }
                    AtomicOrbital morb = new AtomicOrbital(atype, otype, anum, coeffs);
                    lstOrb.add(morb);
                }
                else
                {
                    AtomicOrbital morb = lstOrb.get(idx);
                    for (int i=3; i<vals.length; i++)
                    {
                        morb.getCoeffs().add(Double.parseDouble(vals[i]));
                    }
                }
            }
        }
    }

//------------------------------------------------------------------------------

    private int getIndex(int anum, String atype, String otype)
    {
        if (lstOrb.isEmpty())
        {
            return -1;
        }

        int idx = -1;
        for (int i=0; i<lstOrb.size(); i++)
        {
            AtomicOrbital morb = lstOrb.get(i);
            if (morb.getAtomNum() == anum && morb.getAtomType().equals(atype) &&
                    morb.getOrbitalType().equals(otype))
            {
                idx = i;
            }
        }
        return idx;
    }


//------------------------------------------------------------------------------

    public ArrayList<Double> getAtomCharges()
    {
        return atom_charges;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getVibrationalFrequencies()
    {
        return vibFreq;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getOrbitalEnergies()
    {
        return moEnergies;
    }

//------------------------------------------------------------------------------

    public ArrayList<Point3d> getAtomCoordinates()
    {
        return atom_coords;
    }

//------------------------------------------------------------------------------

    public double getHOMOEnergy()
    {
        return HOMOEnergy;
    }

//------------------------------------------------------------------------------

    public double getLUMOEnergy()
    {
        return LUMOEnergy;
    }

//------------------------------------------------------------------------------

    public int getStatus()
    {
        return status;
    }

//------------------------------------------------------------------------------

    public double getIonizationPotential()
    {
        return ionizationPotential;
    }

//------------------------------------------------------------------------------

    public double getPointChargeDipole()
    {
        return dipolePointCharge;
    }

//------------------------------------------------------------------------------

    public double getDipoleHybrid()
    {
        return dipoleHybrid;
    }

//------------------------------------------------------------------------------

    public double getTotalDipoleMoment()
    {
        return totalDipoleMoment;
    }

//------------------------------------------------------------------------------

    public String getErrorCode()
    {
        return errorString;
    }

//------------------------------------------------------------------------------

    public ArrayList<String> getAtomSymbols()
    {
        return atom_symbols;
    }

//------------------------------------------------------------------------------

    public double getHOF()
    {
        return kcalToEV(this.heatOfFormation);
    }

//------------------------------------------------------------------------------

    public double getCosmoArea()
    {
        return this.cosmoArea;
    }

//------------------------------------------------------------------------------

    public double getCosmoVolume()
    {
        return this.cosmoVolume;
    }

//------------------------------------------------------------------------------

    public double getMW()
    {
        return this.molecularWeight;
    }

//------------------------------------------------------------------------------

    public double getResonanceEnergy()
    {
        return this.resonanceEnergy;
    }

//------------------------------------------------------------------------------

    public double getExchangeEnergy()
    {
        return this.exchangeEnergy;
    }

//------------------------------------------------------------------------------

    public double getTotalEnergy()
    {
        return this.totalEnergy;
    }

//------------------------------------------------------------------------------

    public double getTotalNucleophilicDelocalizability()
    {
        return this.totalNucleophilicDelocalizability;
    }

//------------------------------------------------------------------------------

    public double getTotalElectrophilicDelocalizability()
    {
        return this.totalElectrophilicDelocalizability;
    }

//------------------------------------------------------------------------------

    public double getTotalSelfPolarizability()
    {
        return this.totalSelfPolarizability;
    }

//------------------------------------------------------------------------------

    public double getAbsoluteHardness()
    {
        return this.absoluteHardness;
    }

//------------------------------------------------------------------------------

    public double getSoftness()
    {
        return (0.5/this.absoluteHardness);
    }

//------------------------------------------------------------------------------

    public double getTotalElectrostaticInteraction()
    {
        return this.totalElectrostaticInteraction;
    }

//------------------------------------------------------------------------------

    public double getElectronNuclearAttraction()
    {
        return this.electronNuclearAttraction;
    }

//------------------------------------------------------------------------------

    public double getElectronElectronRepulsion()
    {
        return this.electronElectronRepulsion;
    }

//------------------------------------------------------------------------------

    public double getCoreCoreRepulsion()
    {
        return this.coreCoreRepulsion;
    }

//------------------------------------------------------------------------------

    public double getElectronicEnergy()
    {
        return this.electronicEnergy;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getAtomSelfPolarizabilities()
    {
        return atom_selfpol;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getPolarizabilities()
    {
        return this.polar_desc;
    }


//------------------------------------------------------------------------------

    public ArrayList<Double> getAtomChargeDensities()
    {
        return atom_chgden;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getElectrophilicDelocalizabilities()
    {
        return atom_der;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getNucleophilicDelocalizabilities()
    {
        return atom_dnr;
    }
    
//------------------------------------------------------------------------------

    public String getPointGroup()
    {
        return this.pointGrp;
    }

//------------------------------------------------------------------------------

    public Matrix getBondMatrix()
    {
        return bndMatrix;
    }

//------------------------------------------------------------------------------

    /**
     * An important stability index, a large GAP being related to the high
     * stability of a molecule with its low reactivity in chemical reactions
     * @return HLGap
     */

    public double getHLGap()
    {
        return LUMOEnergy - HOMOEnergy;
    }

//------------------------------------------------------------------------------

    /**
     * Low values of H/L are related to high stability of the molecule.
     * @return HLFraction
     */

    public double getHLFraction()
    {
        return HOMOEnergy/LUMOEnergy;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getTransitionDipoles()
    {
        return this.tdip;
    }


//------------------------------------------------------------------------------

    public ArrayList<Double> getESPCharges()
    {
        return this.esp_charges;
    }

//------------------------------------------------------------------------------

    public double getPIMX()
    {
        return PIMX;
    }

//------------------------------------------------------------------------------

    public double getPIMY()
    {
        return PIMY;
    }

//------------------------------------------------------------------------------

    public ArrayList<AtomicOrbital> getAtomicOrbitals()
    {
        return lstOrb;
    }

//------------------------------------------------------------------------------

    public int getFilledLevels()
    {
        return numFilledLevels;
    }

//------------------------------------------------------------------------------

    public double getPIMZ()
    {
        return PIMZ;
    }

//------------------------------------------------------------------------------

    private double kcalToEV(double val)
    {
        return val * 0.043363;
    }

//------------------------------------------------------------------------------

    public double getElectrophilicity()
    {
        double electrophilicity = ((HOMOEnergy * HOMOEnergy) +
                2 * HOMOEnergy * LUMOEnergy + (LUMOEnergy * LUMOEnergy))/
                (4 * (LUMOEnergy - HOMOEnergy));
        return electrophilicity;
    }

//------------------------------------------------------------------------------

    /**
     * clean up vectors
     */
    
    public void cleanup()
    {
        if (thermo_energies != null)
            thermo_energies.clear();
        if (polar_desc != null)
            polar_desc.clear();
        if (atom_selfpol != null)
            atom_selfpol.clear();
        if (atom_chgden != null)
            atom_chgden.clear();
        if (atom_dnr != null)
            atom_dnr.clear();
        if (atom_der != null)
            atom_der.clear();
        if (atom_symbols != null)
            atom_symbols.clear();
        if (atom_charges != null)
            atom_charges.clear();
        if (atom_coords != null)
            atom_coords.clear();
        if (moEnergies != null)
            moEnergies.clear();
        if (vibFreq != null)
            vibFreq.clear();
        if (mo_bndctrb != null)
            mo_bndctrb.clear();
        if (esp_charges != null)
            esp_charges.clear();
        if (tdip != null)
            tdip.clear();

        if (lstOrb != null)
        {
            for (int i=0; i<lstOrb.size(); i++)
            {
                lstOrb.get(i).getCoeffs().clear();
            }
            lstOrb.clear();
        }
    }

//------------------------------------------------------------------------------

    /**
     * Electrophilic frontier electron density: the sum of all the squared
     * eigenvectors of p on the HOMO.
     * @param anum
     * @return
     */

    private double getElectrophilicFrontierED(int anum)
    {
        double f = 0;

        for (int i=0; i<lstOrb.size(); i++)
        {
            AtomicOrbital morb = lstOrb.get(i);
            if (morb.getAtomNum() == anum )
            {
                f += morb.getCoeffs().get(this.numFilledLevels-1) *
                        morb.getCoeffs().get(this.numFilledLevels-1);
            }
        }

        //System.err.println("EFED " + this.atom_symbols.get(anum-1) + " " + anum + " " + f);

        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Nucleophilic frontier electron density: the sum of all the squared
     * eigenvectors of p on the LUMO.
     *
     * @param anum
     * @return
     */
    private double getNucleophilicFrontierED(int anum)
    {
        double f = 0;

        for (int i=0; i<lstOrb.size(); i++)
        {
            AtomicOrbital morb = lstOrb.get(i);
            if (morb.getAtomNum() == anum )
            {
                f += morb.getCoeffs().get(this.numFilledLevels) *
                        morb.getCoeffs().get(this.numFilledLevels);
            }
        }

        //System.err.println("NFED " + this.atom_symbols.get(anum-1) + " " + anum + " " + f);

        return f;
    }
    
//------------------------------------------------------------------------------

    public double getAveragePolarizability()
    {
        return averagePolarizability;
    }
    

//------------------------------------------------------------------------------

    /**
     *
     * @return polarizability anisotropy
     */

    public double getPolarizabilityAnisotropy()
    {
        double f = 0;
	if (alpha_polar != null && alpha_polar.size() > 0)
	{
            f = ((alpha_polar.get(0)*alpha_polar.get(0)) + (alpha_polar.get(4)*alpha_polar.get(4)) 
                    + (alpha_polar.get(8)*alpha_polar.get(8)) - 
                    3 * polar_desc.get(0) * polar_desc.get(0))/(6 * polar_desc.get(0) * polar_desc.get(0));
	}

        return f;
    }


//------------------------------------------------------------------------------

    public ArrayList<ThermVals> getThermoEnergies()
    {
        return thermo_energies;
    }
    
//------------------------------------------------------------------------------    

}


