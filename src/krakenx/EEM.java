package krakenx;

import Jama.LUDecomposition;
import Jama.Matrix;
import javax.vecmath.Point3d;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

/**
 * Port of the EEM implementation in Open Babel. Based on the EEM charges from 
 * Bultinck.
 * @author Vishwesh Venkatraman
 */
public class EEM
{
    
//------------------------------------------------------------------------------

    private static double[] EEMSolver(double[][] ETA, double[] CHI, int dim) 
    {
        Matrix M = new Matrix(ETA);
        LUDecomposition LU = new LUDecomposition(M);
        
        Matrix B = new Matrix(CHI, dim);
        Matrix C = LU.solve(B);
        return C.getColumnPackedCopy();
    }
    
//------------------------------------------------------------------------------
    
    /**
     * Compute EEM charges. The charges are stored in the IAtomContainer object
     * @param mol atom container
     * @throws Exception 
     */
    public static void computeEEMCharges(IAtomContainer mol) throws Exception
    {
        int natm = mol.getAtomCount();
        int dim = natm+1;
        double[] CHI = new double[dim];
        double[][] ETA = new double[dim][dim];

        double hardness;
        double electronegativity;
        int n = -1;

        for (int i = 0; i < natm; i++)
        {
            IAtom atm = mol.getAtom(i);
            n = atm.getAtomicNumber();

            switch (n) // this should move to a parameter file
            {
            case 1: // H
                hardness = 0.65971;
                electronegativity = 0.20606;
                break;
            case 3: // Li
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 5: // B
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 6: // C
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 7: // N
                hardness = 0.34519;
                electronegativity = 0.49279;
                break;
            case 8: // O
                hardness = 0.54428;
                electronegativity = 0.73013;
                break;
            case 9: // F
                hardness = 0.72664;
                electronegativity = 0.72052;
                break;
            case 11: // Na
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 12: // Mg
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 14: // Si
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 15: // P
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 16: // S
                hardness = 0.20640;
                electronegativity = 0.62020;
                break;
            case 17: // Cl
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 19: // K
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 20: // Ca
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 26: // Fe
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 29: // Cu
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 30: // Zn
                hardness = 0.32966;
                electronegativity = 0.36237;
                break;
            case 35: // Br
                hardness = 0.54554;
                electronegativity = 0.70052;
                break;
            case 53: // I
                hardness = 0.30664;
                electronegativity = 0.68052;
                break;
            default:
                hardness = 0.65971;
                electronegativity = 0.20606;
                break;
            }

            CHI[i] = -electronegativity;
            ETA[i][i] = 2.0 * hardness;
        }

        double totalCharge = AtomContainerManipulator.getTotalFormalCharge(mol);

        CHI[natm] = totalCharge;

        // Complete ETA

        IAtom rAtom = null, cAtom = null;
        double d = 0;
        Point3d c1 = null, c2 = null;
        for (int r = 0; r < natm; r++)
        {
            rAtom = mol.getAtom(r); // Atom index
            if (rAtom.getPoint3d() == null)
            {
                c1 = new Point3d(rAtom.getPoint2d().x, rAtom.getPoint2d().y, 0.0);
            }
            else 
                c1 = rAtom.getPoint3d();
            for (int c = r + 1; c < natm; c++)
            {
                cAtom = mol.getAtom(c); // Atom index
                if (rAtom.getPoint3d() == null)
                {
                    c2 = new Point3d(cAtom.getPoint2d().x, cAtom.getPoint2d().y, 0.0);
                }
                else 
                    c2 = cAtom.getPoint3d();                
                d = Utils.distance (c1, c2);
                ETA[r][c] = 0.529176 / d; // 0.529176: Angstrom to au
                ETA[c][r] = ETA[r][c];
            }
        }

        for (int i = 0; i < dim; i++)
        {
            ETA[i][natm] = -1.0;
            ETA[natm][i] = +1.0;
        }
        ETA[natm][natm] = 0.0;

        double[] charges = EEMSolver(ETA, CHI, dim);

        for (int i=0; i<mol.getAtomCount(); i++)
        {
            mol.getAtom(i).setCharge(charges[i]);
            //System.out.println(String.format("%8.4f", charges[i]));
        }
    }

//------------------------------------------------------------------------------

}
