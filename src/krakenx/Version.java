package krakenx;

import java.util.Properties;

/**
 * Version numbering info
 * @author VISHWESH
 */
public class Version 
{
    public static final String name = "KRAKENX";
    public static final String version = "0.1.4";
    public static final String copyright = "2016";
    public static final String author = "Vishwesh Venkatraman";
    public static final String contributors = "";
    public static final String contact = "bjorn.k.alsberg@ntnu.no";
    public static final String date = "Feb 21, 2016";
    public static final String minimumJavaVersion = "1.5";
    
//------------------------------------------------------------------------------    

    public static String message()
    {
        Properties p = System.getProperties();
        String javaVersion = p.getProperty("java.version");
        String javaVM = p.getProperty("java.vm.name");
        String javaVMVersion = p.getProperty("java.vm.version");
        if (javaVM != null) 
            javaVersion = javaVersion + " / " + javaVM;
        if (javaVM != null && javaVMVersion != null)
            javaVersion = javaVersion + "-" + javaVMVersion;
    
        return 
            "\n| " + name + 
            "\n| KRAKENX: Software for Generation of Alignment Independent 3D Descriptors (version " + version + ")" +
            "\n| By " + author + 
            "\n| Contributors: " + contributors +
            "\n| Contact: " + contact + 
            "\n| Date: " + date +
            "\n| Current Java: " + javaVersion +
            "\n| Required Minimum Java: " + minimumJavaVersion +
            "\n\n";
    }
    
//------------------------------------------------------------------------------        
}
