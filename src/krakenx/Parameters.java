package krakenx;

/**
 * Utility to parse the parameter file
 * @author VISHWESH
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Parameters
{
    private String lstMopFileName = "";
    private String lstSDFFileName = "";
    private String lstCOSMOFileName = "";
    private String outFileName = "";

    private String lstChargeFileName = "";

    private HashMap<String, Boolean> lstDesc;
    private HashMap<String, Boolean> lstWts;
    private HashMap<String, Integer> lstPointGroup;

    private HashMap<String, ArrayList<Double>> lstEVAPars;
    private HashMap<String, ArrayList<Double>> lstEEVAPars;
    private HashMap<String, ArrayList<Double>> lstCEVAPars;
    private double solventRad = 1.4;

    private String ChargeType = "MOPAC";
    private String AtomTyping = "SYBYL";
    
    private String OptimType = "AM1";
    
    private String prefix = ""; // prefix descriptor names with a string

    private double RDFBETA = 100;

    private String mopacLocn = "";

    private ArrayList<String> lstMopFiles = null;
    private ArrayList<String> lstCOSMOFiles = null;
    

    private ArrayList<String> lstSDFFiles = null;
    private ArrayList<String> lstUDFChargeFiles = null;
    
    private double RTEMP = 298.15;
    
    private final String[] HAMILTONIANS = {"AM1", "PM3", "PM6", "PM7", "RM1"};

//------------------------------------------------------------------------------

    public void initialize()
    {
        lstDesc = new HashMap<>();
        lstWts = new HashMap<>();        
        lstPointGroup = new HashMap<>();

        lstDesc.put("COSMO", Boolean.FALSE);
        lstDesc.put("EVA", Boolean.FALSE);
        lstDesc.put("EEVA", Boolean.FALSE);
        lstDesc.put("RDF", Boolean.FALSE);
        lstDesc.put("MORSE", Boolean.FALSE);
        lstDesc.put("WHIM", Boolean.FALSE);
        lstDesc.put("MOPAC", Boolean.FALSE);
        lstDesc.put("CPSA", Boolean.FALSE);
        lstDesc.put("CHARGE", Boolean.FALSE);
        lstDesc.put("BCUT", Boolean.FALSE);
        lstDesc.put("AUTOCOR", Boolean.FALSE);
        lstDesc.put("GEOMETRY", Boolean.FALSE);
        lstDesc.put("DIP", Boolean.FALSE);
        lstDesc.put("CEVA", Boolean.FALSE);
        lstDesc.put("GE", Boolean.FALSE);
        lstDesc.put("CMTX", Boolean.FALSE);

        lstWts.put("CHARGE", Boolean.FALSE);
        lstWts.put("SPOL", Boolean.FALSE);
        lstWts.put("NDLOC", Boolean.FALSE);
        lstWts.put("EDLOC", Boolean.FALSE);
        lstWts.put("RDLOC", Boolean.FALSE);
        lstWts.put("CDEN", Boolean.FALSE);
        
        // see Theor Chem Account (2007) 118:813–826; DOI 10.1007/s00214-007-0328-0
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("Cs", 1);
        lstPointGroup.put("C2", 1);
        lstPointGroup.put("C2v", 1);
        lstPointGroup.put("C3v", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        lstPointGroup.put("C1", 1);
        

        lstCEVAPars = new HashMap<>();
        lstEVAPars = new HashMap<>();
        lstEEVAPars = new HashMap<>();
        
    }
    
//------------------------------------------------------------------------------

    public double getSolventRadius()
    {
        return solventRad;
    }
    
//------------------------------------------------------------------------------

    public void setSolventRadius(double rad)
    {
        solventRad = rad;
    }    

//------------------------------------------------------------------------------

    private void setWeights(String wName, boolean status)
    {
        if (lstWts.containsKey(wName))
        {
            lstWts.put(wName, status);
        }
    }

//------------------------------------------------------------------------------

    private void setDescriptorType(String dName, boolean status)
    {
        if (lstDesc.containsKey(dName))
        {
            lstDesc.put(dName, status);
        }
    }

//------------------------------------------------------------------------------

    private boolean checkDescriptorYN(String val)
    {
        if (val.equalsIgnoreCase("Yes"))
            return true;
        if (val.equalsIgnoreCase("Y"))
            return true;
        if (val.equalsIgnoreCase("No"))
            return false;
        if (val.equalsIgnoreCase("N"))
            return false;
        return false;
    }

//------------------------------------------------------------------------------

    /**
     *
     * @param infile
     * @throws KrakenException
     */

    public void readParameters(String infile) throws KrakenException
    {
        String option, line;

        BufferedReader br = null;


        try
        {
            br = new BufferedReader(new FileReader(infile));

            while ((line = br.readLine()) != null)
            {
                if (line.startsWith("#"))
                {
                    continue;
                }

                if ((line.trim()).length() == 0)
                {
                    continue;
                }

                //System.err.println("Line: " + line);

                if (line.toUpperCase().startsWith("LSTMOPFILENAME="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.lstMopFileName = option;
                        this.lstMopFiles = IO.readList(lstMopFileName);
                    }
                }
                
                if (line.toUpperCase().startsWith("LSTCOSFILENAME="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.lstCOSMOFileName = option;
                        this.lstCOSMOFiles = IO.readList(lstCOSMOFileName);
                    }
                }
                
                if (line.toUpperCase().startsWith("SOLVENTRADIUS="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.solventRad = Double.parseDouble(option);                        
                    }
                }

                if (line.toUpperCase().startsWith("LSTCHARGEFILENAME="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.lstChargeFileName = option;
                        this.ChargeType = "UDF";

                        this.lstUDFChargeFiles = IO.readList(lstChargeFileName);
                    }
                }
                
                if (line.toUpperCase().startsWith("PREFIX="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.prefix = option;
                    }
                }
                
                if (line.toUpperCase().startsWith("COULOMBMATRIX="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("CMTX", checkDescriptorYN(option));
                    }
                }
                
                if (line.toUpperCase().startsWith("Temperature="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.RTEMP = Double.parseDouble(option);
                    }
                }
                

                if (line.toUpperCase().startsWith("LSTSDFFILENAME="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.lstSDFFileName = option;
                        this.lstSDFFiles = IO.readList(lstSDFFileName);
                    }
                }
                
                
                if (line.toUpperCase().startsWith("MOPACLOCN="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.mopacLocn = option;
                    }
                }
                
                if (line.toUpperCase().startsWith("MOPACHAMILTONIAN="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.OptimType = option;
                    }
                }

                if (line.toUpperCase().startsWith("OUTPUTFILENAME="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        this.outFileName = option;
                    }
                }


                if (line.toUpperCase().startsWith("CEVA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("CEVA", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("FFTYPE="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        AtomTyping = option;
                    }
                }

                if (line.toUpperCase().startsWith("AT_"))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        String[] vals = option.split(",");                        
                        ArrayList<Double> prs = new ArrayList<>();
                        for (String str:vals)
                            prs.add(Double.valueOf(str));
                        String atype = line.substring(0, line.indexOf("=")).trim();
                        //System.err.println(atype + " " + Arrays.toString(vals));
                        lstCEVAPars.put(atype, prs);
                    }
                }


                if (line.toUpperCase().startsWith("EVA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("EVA", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("EVASIGMA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        String[] vals = option.split(",");
                        ArrayList<Double> X = new ArrayList<>();
                        for (String str:vals)
                        {
                            X.add(Double.parseDouble(str));
                        }
                        lstEVAPars.put("SIGMA", X);
                    }
                }



                if (line.toUpperCase().startsWith("EVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        String[] vals = option.split(",");
                        ArrayList<Double> X = new ArrayList<>();
                        for (String str:vals)
                        {
                            X.add(Double.parseDouble(str));
                        }
                        lstEVAPars.put("L", X);
                    }
                }

                if (line.toUpperCase().startsWith("EVAMINVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        ArrayList<Double> X = new ArrayList<>();
                        X.add(Double.parseDouble(option));
                        lstEVAPars.put("MINVAL", X);

                    }
                }

                if (line.toUpperCase().startsWith("EVAMAXVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        ArrayList<Double> X = new ArrayList<>();
                        X.add(Double.parseDouble(option));
                        lstEVAPars.put("MAXVAL", X);
                    }
                }

                if (line.toUpperCase().startsWith("EEVA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("EEVA", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("EEVASIGMA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        String[] vals = option.split(",");
                        ArrayList<Double> X = new ArrayList<>();
                        for (String str:vals)
                        {
                            X.add(Double.parseDouble(str));
                        }
                        lstEEVAPars.put("SIGMA", X);
                    }
                }

                if (line.toUpperCase().startsWith("EEVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        String[] vals = option.split(",");
                        ArrayList<Double> X = new ArrayList<>();
                        for (String str:vals)
                        {
                            X.add(Double.parseDouble(str));
                        }
                        lstEEVAPars.put("L", X);
                    }
                }

                if (line.toUpperCase().startsWith("EEVAMINVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        ArrayList<Double> X = new ArrayList<>();
                        X.add(Double.parseDouble(option));
                        lstEEVAPars.put("MINVAL", X);
                    }
                }

                if (line.toUpperCase().startsWith("EEVAMAXVAL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        ArrayList<Double> X = new ArrayList<>();
                        X.add(Double.parseDouble(option));
                        lstEEVAPars.put("MAXVAL", X);
                    }
                }

                if (line.toUpperCase().startsWith("RDF="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("RDF", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("RDFBETA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                          this.RDFBETA = Double.parseDouble(option);
                    }
                }

                if (line.toUpperCase().startsWith("MORSE="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("MORSE", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("DIP="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("DIP", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("WHIM="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("WHIM", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("BCUT="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("BCUT", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("CPSA="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("CPSA", checkDescriptorYN(option));
                    }
                }
                
                if (line.toUpperCase().startsWith("GRAPHENERGY="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("GE", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("AUTOCORRELATION="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("AUTOCOR", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("GEOMETRY="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("GEOMETRY", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("MOPAC="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        setDescriptorType("MOPAC", checkDescriptorYN(option));
                    }
                }

                if (line.toUpperCase().startsWith("CHARGEDESC="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setDescriptorType("CHARGE", checkDescriptorYN(option));
                }

                if (line.toUpperCase().startsWith("CHARGETYPE="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    if (option.length() > 0)
                    {
                        ChargeType = option.toUpperCase();
                    }
                }

                if (line.toUpperCase().startsWith("CHARGE="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("CHARGE", checkDescriptorYN(option));
                }


                // Weights

                if (line.toUpperCase().startsWith("SELFPOL="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("SPOL", checkDescriptorYN(option));
                }

                if (line.toUpperCase().startsWith("NUCLEARDELOC="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("NDLOC", checkDescriptorYN(option));
                }

                if (line.toUpperCase().startsWith("ELECTROPHILICDELOC="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("EDLOC", checkDescriptorYN(option));
                }

                if (line.toUpperCase().startsWith("RADICALDELOC="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("RDLOC", checkDescriptorYN(option));
                }
                
                if (line.toUpperCase().startsWith("CHGDEN="))
                {
                    option = line.substring(line.indexOf("=") + 1).trim();
                    setWeights("CDEN", checkDescriptorYN(option));
                }
            }
        }
        catch (NumberFormatException | IOException nfe)
        {
            throw new KrakenException(nfe);
        }
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }
    }

//------------------------------------------------------------------------------

    public String getChargeScheme()
    {
        return ChargeType;
    }
    
//------------------------------------------------------------------------------

    public String getAtomTyping()
    {
        return AtomTyping;
    }    

//------------------------------------------------------------------------------

    public String getLstMopFileName()
    {
        return lstMopFileName;
    }
    
//------------------------------------------------------------------------------

    public String getLstCOSMOFileName()
    {
        return lstCOSMOFileName;
    }    

//------------------------------------------------------------------------------

    public String getLstSDFFileName()
    {
        return lstSDFFileName;
    }

//------------------------------------------------------------------------------

    public String getOutFileName()
    {
        return outFileName;
    }

//------------------------------------------------------------------------------

    public HashMap<String, ArrayList<Double>> getEEVAPars()
    {
        return lstEEVAPars;
    }

//------------------------------------------------------------------------------

    public HashMap<String, ArrayList<Double>> getEVAPars()
    {
        return lstEVAPars;
    }
    
//------------------------------------------------------------------------------

    public HashMap<String, ArrayList<Double>> getCEVAPars()
    {
        return lstCEVAPars;
    }    

//------------------------------------------------------------------------------

    public double getRDFBETA()
    {
        return RDFBETA;
    }

//------------------------------------------------------------------------------

    /**
     * Check parameters
     * @throws KrakenException
     */

    public void checkParameters() throws KrakenException
    {
        if (lstSDFFileName.length() == 0)
        {
            throw new KrakenException("No SDF files provided.");
        }
        
        if (this.lstSDFFiles.isEmpty())
        {
            throw new KrakenException("Require a list of SDF files to be supplied.");
        }
        
        if (mopacLocn.isEmpty())
        {
            if (lstMopFileName.isEmpty())
            {
                throw new KrakenException("Require a list of MOPAC output files to be supplied.");
            }
        }
        else 
        {
            if (!mopacLocn.isEmpty())
            {
                if (!new File(mopacLocn).exists())
                {
                    throw new KrakenException("Could not locate " + mopacLocn);
                }
                
                if (OptimType.length() == 0)
                {
                    throw new KrakenException("Must provide the Hamiltonian to be used.");
                }

                if (!Arrays.asList(HAMILTONIANS).contains(OptimType))
                {
                    throw new KrakenException("Incorrect Hamiltonian provided. "
                            + "Choose from " + Arrays.toString(HAMILTONIANS));
                }
            }
        }
        

        Iterator<Map.Entry<String, Boolean>> iterator = lstDesc.entrySet().iterator();

        int lc = 0, wtc = 0;
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            //System.err.println(entry.getKey() + " " + entry.getValue());
            if (!entry.getValue())
                lc++;
            
            if (lstDesc.get("RDF"))
                wtc++;
            if (lstDesc.get("WHIM"))
                wtc++;
            if (lstDesc.get("BCUT"))
                wtc++;
            if (lstDesc.get("MORSE"))
                wtc++;
            if (lstDesc.get("AUTOCOR"))
                wtc++;
            if (lstDesc.get("GE"))
                wtc++;
        }

        if (lc == lstDesc.size())
            throw new KrakenException("No descriptor type selected.");


        // check if weights have been selected for RDF,MORSE,WHIM,BCUT, 
        // graph energy and auto/cross correlation
        if (wtc > 0)
        {
            //System.err.println("WTC" + wtc);
            Iterator<Map.Entry<String, Boolean>> wTiterator = lstWts.entrySet().iterator();
            int wc = 0;
            while (wTiterator.hasNext())
            {
                Map.Entry<String, Boolean> wTentry = wTiterator.next();
                //System.err.println(wTentry.getKey() + " " + wTentry.getValue());
                
                if (wTentry.getValue())
                {
                    wc++;
                }
            }

            if (wc == 0)
            {
                throw new KrakenException("No atom weighting scheme selected for any of "
                + "RDF/WHIM/MORSE/Autocorrelation/Graph energy/BCUT");
            }
        }

        

        if (ChargeType.length() == 0)
        {
            throw new KrakenException("Undefined charge scheme.");
        }

        if (ChargeType.length() > 0)
        {
            boolean chargeTypeOK = false;
            if (ChargeType.equalsIgnoreCase("MOPAC"))
                chargeTypeOK = true;
            if (ChargeType.equalsIgnoreCase("EEM"))
                chargeTypeOK = true;
            if (ChargeType.equalsIgnoreCase("UDF"))
                chargeTypeOK = true;
            if (ChargeType.equalsIgnoreCase("ESP"))
                chargeTypeOK = true;

            if (!chargeTypeOK)
            {
                throw new KrakenException("Undefined charge scheme.");
            }

            if (ChargeType.equals("UDF"))
            {

                if (this.lstUDFChargeFiles.isEmpty())
                {
                    throw new KrakenException("Require a list of files containing "
                            + "user-defined charges for each molecule to be supplied.");
                }
            }
        }

        
    }

//------------------------------------------------------------------------------

    public ArrayList<String> getLstMopFiles()
    {
        return lstMopFiles;
    }

//------------------------------------------------------------------------------

    public ArrayList<String> getLstCOSMOFiles()
    {
        return this.lstCOSMOFiles;
    }    

//------------------------------------------------------------------------------

    public ArrayList<String> getLstSDFFiles()
    {
        return lstSDFFiles;
    }

//------------------------------------------------------------------------------

    public ArrayList<String> getLstUDFChargeFiles()
    {
        return lstUDFChargeFiles;
    }

//------------------------------------------------------------------------------

    public String getUDFChargeFileName()
    {
        return lstChargeFileName;
    }

//------------------------------------------------------------------------------

    public void setUDFChargeFileName(String udfChargeFile)
    {
        this.lstChargeFileName = udfChargeFile;
    }

//------------------------------------------------------------------------------

    public HashMap<String, Boolean> getLstDesc()
    {
        return lstDesc;
    }

//------------------------------------------------------------------------------

    public HashMap<String, Boolean> getLstWts()
    {
        return lstWts;
    }

//------------------------------------------------------------------------------
    
    public String getMOPACLocation()
    {
        return mopacLocn;
    }
    
//------------------------------------------------------------------------------    
    
    public String getOptimizationType()
    {
        return OptimType;
    }
    
//------------------------------------------------------------------------------        
   
    public String getPrefix()
    {
        return prefix;
    }
    
//------------------------------------------------------------------------------        
   
    public double getRTemp()
    {
        return RTEMP;
    }
    
//------------------------------------------------------------------------------     

}
