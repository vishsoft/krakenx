package krakenx;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.vecmath.Point3d;

import krakenx.descriptors.Autocorrelation;
import krakenx.descriptors.BCUT;
import krakenx.descriptors.CPSA;
import krakenx.descriptors.Charge;
import krakenx.descriptors.CoulombMatrix;
import krakenx.descriptors.DIP;
import krakenx.descriptors.EVA;
import krakenx.descriptors.Geometry;
import krakenx.descriptors.MOPAC;
import krakenx.descriptors.Morse;
import krakenx.descriptors.RDF;
import krakenx.descriptors.WHIM;
import krakenx.descriptors.GraphEnergy;

import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * Main class launching the calculation of the descriptors
 * @author VISHWESH
 */
public class KrakenX
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        if (args.length < 1)
        {
            System.err.println("Usage: java -jar KrakenX paramFile");
            System.exit(-1);
        }

        String paramFile = args[0];

        KrakenX krkn = new KrakenX();

        

        try
        {
            // parameter file parsing
            Parameters prm = new Parameters();
            prm.initialize();
            prm.readParameters(paramFile);
            prm.checkParameters();

            // initialize list of mopac and SDF files to be read in
            ArrayList<String> mopFiles = prm.getLstMopFiles();
            ArrayList<String> sdfFiles = prm.getLstSDFFiles();
            
            // prefix the descriptor name as required
            String prefix = prm.getPrefix();
            if (prefix.length() > 0)
                prefix = prefix + "_";

            boolean append = false;
            boolean header = true;
            boolean created = false;
            
            // working directory where the mopac output files will be stored
            // Applicable if MOPAC used within KRAKENX
            String wrkDir = ""; 
            
            // flag indicating success or failure in reading files, 
            // their completion level etc.
            int status = -1;

            int n = sdfFiles.size();
            
            int maxAtomCnt = 1024;
            if (n > 1)
                maxAtomCnt = getMaxAtomCount(sdfFiles);
            
            // For each molecule
            for (int i=0; i<n; i++)
            {
                IAtomContainer mol = IO.readSingleSDFFile(sdfFiles.get(i));

                // obtain name of molecule
                // assumes all molecule files have a 3 letter extension
                String fname = FilenameUtils.getName(sdfFiles.get(i));
                fname = FilenameUtils.removeExtension(fname);

                System.err.println("Working on " + fname + " ...");

                MOPACReader moprd = new MOPACReader();
                
                // if no mopac files are provided, KRAKENX will attempt to 
                // carry out MOPAC calculations using the MOPAC executable 
                // location provided in the parameter file

                if (mopFiles == null || mopFiles.isEmpty())
                {
                    // create the working directory to store mopac output
                    if (!created)
                    {
                        wrkDir = Utils.createWorkDirectory();
                        if (wrkDir == null)
                        {
                            throw new KrakenException("Failed to create a "
                                    + "working directory. Check permissions.");
                        }
                        created = true;
                    }

                    // store the status of the MOPAC task being run    
                    status = krkn.runTask(fname, wrkDir, mol, prm.getOptimizationType(),
                                            prm.getMOPACLocation());

                    
                    // Will skip the current molecule if it fails for any reason
                    if (status != 0)
                    {
                        System.err.println("Skipping " + fname);
                        continue;
                    }

                    // read the MOPAC output
                    moprd.readMOPACData(wrkDir + File.separator + fname + ".out");

                    if (moprd.getStatus() == -1)
                    {
                        throw new KrakenException("File " + (wrkDir + File.separator + fname + ".out") +
                            " has an error. Exiting ...");
                    }
                }
                else
                {
                    moprd.readMOPACData(mopFiles.get(i));
                    // Check if the output file has any errors. Exit if any
                    if (moprd.getStatus() == -1)
                    {
                        throw new KrakenException("File " + mopFiles.get(i) +
                            " has an error. Exiting ...");
                    }
                }

                // update coordinates
                krkn.setCoordinates(mol, moprd.getAtomCoordinates());

                // update charge
                ArrayList<Double> chgs = null;
                switch (prm.getChargeScheme())
                {
                    case "MOPAC": // default MOPAC charges
                        chgs = moprd.getAtomCharges();
                        krkn.setCharges(mol, chgs);
                        break;
                    case "ESP": // ESP fit charges
                        chgs = moprd.getESPCharges();
                        krkn.setCharges(mol, chgs);
                        break;
                    case "EEM": // EEM charges
                        EEM.computeEEMCharges(mol);
                        break;
                    case "UDF": // other charges calculated externally
                        chgs = IO.readCharges(prm.getLstUDFChargeFiles().get(i));
                        krkn.setCharges(mol, chgs);
                        break;
                    default:
                        throw new KrakenException("Undefined charge scheme.");
                }

                HashMap<String, Boolean> props = prm.getLstDesc();
                Iterator<Map.Entry<String, Boolean>> iterator = props.entrySet().iterator();

                //System.err.println(props.toString());

                ArrayList<DescriptorContainer> lstDesc = new ArrayList<>();

                while (iterator.hasNext())
                {
                    Map.Entry<String, Boolean> entry = iterator.next();
                    if (entry.getValue())
                    {
                        // Vibrational frequency based eigenvalue descriptors
                        if (entry.getKey().equals("EVA"))
                        {
                            System.err.println("Computing EVA...");
                            lstDesc.add(EVA.calculateEVA(prm.getEVAPars(),
                                    moprd.getVibrationalFrequencies(), prefix + "EVA"));
                        }

                        // charge based eigenvalue descriptors
                        if (entry.getKey().equals("CEVA"))
                        {
                            System.err.println("Computing CEVA...");
                            lstDesc.add(EVA.getCEVADescriptors(mol, prm.getCEVAPars(),
                                    prm.getAtomTyping(), prefix + "CEVA"));
                        }

                        // orbital energy based eigenvalue descriptors
                        if (entry.getKey().equals("EEVA"))
                        {
                            System.err.println("Computing EEVA...");
                            lstDesc.add(EVA.calculateEVA(prm.getEEVAPars(),
                                    moprd.getOrbitalEnergies(), prefix + "EEVA"));
                        }
                        
                        // orbital energy based eigenvalue descriptors
                        if (entry.getKey().equals("GE"))
                        {
                            System.err.println("Computing graph energy...");
                            lstDesc.add(GraphEnergy.calculateEigenDescriptors(mol, 
                                prm.getLstWts(), moprd, prefix + "GEIG"));
                        }


                        // Radial Distribution Function descriptors
                        if (entry.getKey().equals("RDF"))
                        {
                            System.err.println("Computing RDF...");
                            lstDesc.add(RDF.calculateRDFDescriptors(mol, prm.getRDFBETA(),
                                        prm.getLstWts(), moprd, prefix + "RDF_"));
                        }
                        
                        if (entry.getKey().equals("CMTX"))
                        {
                            System.err.println("Computing Coulomb Matrix...");
                            lstDesc.add(
                                CoulombMatrix.calculateCoulombMatrix(mol, maxAtomCnt, prefix + "CMTX_"));
                        }


                        // 3D-MoRSE descriptors
                        if (entry.getKey().equals("MORSE"))
                        {
                            System.err.println("Computing MORSE...");
                            lstDesc.add(Morse.calculateMORSEDescriptors(
                                    mol, prm.getLstWts(), moprd, prefix + "MORSE_"));
                        }

                        // charge partial surface area descriptors
                        if (entry.getKey().equals("CPSA"))
                        {
                            System.err.println("Computing CPSA...");
                            lstDesc.add(CPSA.getCPSADescripors(mol, prm.getSolventRadius(), prefix + "CPSA_"));
                        }


                        // 
                        if (entry.getKey().equals("WHIM"))
                        {
                            System.err.println("Computing WHIM...");
                            lstDesc.add(WHIM.calculateWHIMDescriptors(mol, 
                                    prm.getLstWts(), moprd, prefix + "WHIM_"));
                        }


                        if (entry.getKey().equals("BCUT"))
                        {
                            System.err.println("Computing BCUT...");
                            lstDesc.add(BCUT.calculateBCUTDescriptors(mol, 
                                    prm.getLstWts(), moprd, prefix + "BCUT_"));
                        }


                        if (entry.getKey().equals("GEOMETRY"))
                        {
                            System.err.println("Computing GEOMETRY...");
                            lstDesc.add(
                                Geometry.getGeometryDescriptors(mol, moprd, prefix + "GEOM_"));
                        }


                        if (entry.getKey().equals("MOPAC"))
                        {
                            System.err.println("Extracting MOPAC...");
                            lstDesc.add(MOPAC.getDescriptors(moprd, prefix + "MOPAC_", prm.getRTemp()));
                        }

                        if (entry.getKey().equals("DIP"))
                        {
                            System.err.println("Computing DIP...");

                            lstDesc.add(DIP.getDIPDescripors(mol, prefix + "DIP_"));
                        }


                        if (entry.getKey().equals("AUTOCOR"))
                        {
                            System.err.println("Computing AUTOCORRELATION...");

                            lstDesc.add(Autocorrelation.getAutocorrelationDescriptors(mol,
                                        prm.getLstWts(), moprd, prefix + "ACOR_"));
                        }

                        if (entry.getKey().equals("CHARGE"))
                        {
                            System.err.println("Computing CHARGE descriptors ...");
                            lstDesc.add(Charge.getChargeDescriptors(mol, moprd, prefix + "CHG_"));
                        }
                    }
                }

                IO.writeDescriptors(prm.getOutFileName(), fname, lstDesc, header, append);
                append = true;
                header = false;

                // cleanup data structures
                cleanup(lstDesc);
                moprd.cleanup();
                if (chgs != null)
                    chgs.clear();
            }
        }
        catch (KrakenException kex)
        {
            printExceptionChain(kex);
            System.exit(-1);
        }
        catch (Exception ex)
        {
            printExceptionChain(ex);
            System.exit(-1);
        }

        System.exit(0);

    }

//------------------------------------------------------------------------------
    
    /**
     * Cleanup arrays and lists
     * @param lstDesc 
     */

    private static void cleanup(ArrayList<DescriptorContainer> lstDesc)
    {
        for (DescriptorContainer dc:lstDesc)
            dc.cleanup();
        lstDesc.clear();
    }

//------------------------------------------------------------------------------

    /**
     * Utility function that prints the chain of exceptions
     * @param thr 
     */
    public static void printExceptionChain(Throwable thr)
    {

        StackTraceElement elements[] = thr.getStackTrace();
        for (int i = 0, n = elements.length; i < n; i++)
        {
            System.err.println(elements[i].getFileName() +  ":"
                            + elements[i].getLineNumber() + ">> "
                            + elements[i].getMethodName() + "()"
                            );
        }


        StackTraceElement[] steArr;
        Throwable cause = thr;
        while (cause != null)
        {
            System.err.println("-------------------------------");
            steArr = cause.getStackTrace();
            StackTraceElement s0 = steArr[0];
            System.err.println(cause.toString());
            System.err.println("  at " + s0.toString());
            if (cause.getCause() == null)
            {
                System.err.println("-------------------------------");
                cause.printStackTrace();
            }
            cause = cause.getCause();
        }
    }

//------------------------------------------------------------------------------

    /**
     * Set atom charges 
     * @param mol
     * @param atom_charges
     */

    private void setCharges(IAtomContainer mol, ArrayList<Double> atom_charges)
    {
        int i = 0;
        for (IAtom atm:mol.atoms())
        {
            atm.setCharge(atom_charges.get(i));
            i++;
        }
    }

//------------------------------------------------------------------------------

    /**
     * Set atom coordinates to the optimized version
     * @param mol
     * @param atom_coords
     */

    private void setCoordinates(IAtomContainer mol, ArrayList<Point3d> atom_coords)
    {
        int i = 0;
        for (IAtom atm:mol.atoms())
        {
            atm.setPoint3d(atom_coords.get(i));
            i++;
        }
    }

//------------------------------------------------------------------------------
    
    /**
     * Run an external task (currently mopac)
     * @param fname name of the file 
     * @param wrkDir the working directory
     * @param mol the molecule
     * @param opttype optimization type
     * @param strExe the executable to be used
     * @return status code of the execution
     * @throws Exception 
     */

    private int runTask(String fname, String wrkDir, IAtomContainer mol,
                            String opttype, String strExe) throws Exception
    {
        // perform MOPAC optimization
        String mopfile = wrkDir + File.separator + fname + ".mop";
        IO.writeMOPACFile(mopfile, mol, opttype);

        String cmdStr = strExe + " " + mopfile;


        ProcessHandler ph = new ProcessHandler(cmdStr);
        ph.runProcess();
        if (ph.getExitCode() != 0)
        {
            System.err.println("Error: " + cmdStr);
        }

        return ph.getExitCode();
    }
    
//------------------------------------------------------------------------------

    private static int getMaxAtomCount(ArrayList<String> sdfFiles) throws KrakenException
    {
        int n = 0;
        for (int i=0; i<sdfFiles.size(); i++)
        {
            IAtomContainer mol = IO.readSingleSDFFile(sdfFiles.get(i));
            if (mol.getAtomCount() > n)
                n = mol.getAtomCount();
        }
        
        return n;
    }
    

//------------------------------------------------------------------------------

}
