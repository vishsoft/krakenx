package krakenx.descriptors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.ThermVals;

/**
 *
 * @author Vishwesh Venkatraman
 */
public class MOPAC
{
    private static final HashMap<String, Integer> POINTGROUP = new HashMap<>();
    static
    {
        // see Theor Chem Account (2007) 118:813–826; DOI 10.1007/s00214-007-0328-0
        POINTGROUP.put("C1", 1);
        POINTGROUP.put("Cs", 1);
        POINTGROUP.put("C2", 2);
        POINTGROUP.put("C2v", 2);
        POINTGROUP.put("C3v", 3);
        POINTGROUP.put("D2h", 4);
        POINTGROUP.put("D3h", 6);
        POINTGROUP.put("D5h", 10);
        POINTGROUP.put("D3d", 6);
        POINTGROUP.put("Td", 12);
        POINTGROUP.put("Oh", 24);
        POINTGROUP.put("D*h", 2);
        POINTGROUP.put("C*v", 1);
    }
    
    public static DescriptorContainer getDescriptors(MOPACReader moprd, 
            String prefix, double RTEMP)
    {
        // split BFS
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        desc.add(moprd.getCosmoArea());
        desc.add(moprd.getCosmoVolume());
        desc.add(moprd.getHOMOEnergy());
        desc.add(moprd.getLUMOEnergy());
        desc.add(moprd.getHLGap());
        desc.add(moprd.getHLFraction());
        desc.add(moprd.getPointChargeDipole());
        desc.add(moprd.getDipoleHybrid());
        desc.add(moprd.getTotalDipoleMoment());
        desc.add(moprd.getHOF());
        desc.add(moprd.getAbsoluteHardness());
        desc.add(moprd.getSoftness());
        desc.add(moprd.getCoreCoreRepulsion());
        desc.add(moprd.getTotalEnergy());
        desc.add(moprd.getElectronNuclearAttraction());
        desc.add(moprd.getElectronElectronRepulsion());
        desc.add(moprd.getElectronicEnergy());
        desc.add(moprd.getResonanceEnergy());
        desc.add(moprd.getExchangeEnergy());
        desc.add(moprd.getTotalElectrostaticInteraction());
        desc.add(moprd.getTotalElectrophilicDelocalizability());
        desc.add(moprd.getTotalNucleophilicDelocalizability());
        desc.add(moprd.getTotalSelfPolarizability()); // sum of self-atom polarizabilities
        desc.add(moprd.getElectrophilicity());
        desc.add(moprd.getPIMX());
        desc.add(moprd.getPIMY());
        desc.add(moprd.getPIMZ());
        desc.add(moprd.getMW());
        desc.add((double) moprd.getFilledLevels());
        desc.add(getMinValue(moprd.getNucleophilicDelocalizabilities()));
        desc.add(getMinValue(moprd.getElectrophilicDelocalizabilities()));
        desc.add(getMinValue(moprd.getAtomSelfPolarizabilities()));
        desc.add(getMaxValue(moprd.getNucleophilicDelocalizabilities()));
        desc.add(getMaxValue(moprd.getElectrophilicDelocalizabilities()));
        desc.add(getMaxValue(moprd.getAtomSelfPolarizabilities()));
        desc.add(moprd.getAveragePolarizability());
        desc.add(moprd.getPolarizabilityAnisotropy());
        desc.add(evaluateGroup(moprd.getPointGroup()));


        names.add(prefix + "COSMO_AREA");
        names.add(prefix + "COSMO_VOLUME");
        names.add(prefix + "HOMO");
        names.add(prefix + "LUMO");
        names.add(prefix + "HLGAP");
        names.add(prefix + "HLFRACTION");
        names.add(prefix + "CHARGE_DIPOLE");
        names.add(prefix + "HYBRID_DIPOLE");
        names.add(prefix + "TOTAL_DIPOLE");
        names.add(prefix + "HOF");
        names.add(prefix + "ABSOLUTE_HARDNESS");
        names.add(prefix + "TOTAL_SOFTNESS"); // 1/( Parr and Pople Absolute Hardness)
        names.add(prefix + "CORE-CORE_REPULSION");
        names.add(prefix + "TOTAL_ENERGY");
        names.add(prefix + "ELEC_NUC_ATTR");
        names.add(prefix + "ELEC_ELEC_REPL");
        names.add(prefix + "ELEC_ENERGY");
        names.add(prefix + "RESONANCE_ENERGY");
        names.add(prefix + "EXCHG_ENERGY");
        names.add(prefix + "TOTAL_ELEC_INTRN");
        names.add(prefix + "TOTAL_EPHIL_DELOC");
        names.add(prefix + "TOTAL_NPHIL_DELOC");
        names.add(prefix + "TOTAL_SPOL");
        names.add(prefix + "ELECTROPHILICITY");
        names.add(prefix + "PIMX");
        names.add(prefix + "PIMY");
        names.add(prefix + "PIMZ");
        names.add(prefix + "MW");
        names.add(prefix + "FILLEDLEVELS");
        names.add(prefix + "MIN_DNR");
        names.add(prefix + "MIN_DER");
        names.add(prefix + "MIN_SPOL");
        names.add(prefix + "MAX_DNR");
        names.add(prefix + "MAX_DER");
        names.add(prefix + "MAX_SPOL");
        names.add(prefix + "AVG_POLARIZABILITY");
        names.add(prefix + "POLARANISOTROPY");
        names.add(prefix + "POINTGROUP");

        names.add(prefix + "VIB_ENTHALPY");
        names.add(prefix + "VIB_HEATCAP");
        names.add(prefix + "VIB_ENTROPY");
        names.add(prefix + "ROT_ENTHALPY");
        names.add(prefix + "ROT_HEATCAP");
        names.add(prefix + "ROT_ENTROPY");
        names.add(prefix + "TRANS_ENTHALPY");
        names.add(prefix + "TRANS_HEATCAP");
        names.add(prefix + "TRANS_ENTROPY");
        names.add(prefix + "THERM_HEAT_OF_FORMATION");

        if (moprd.getThermoEnergies().isEmpty())
        {
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
        }
        else
        {
            int idx = findClosest(moprd.getThermoEnergies(), RTEMP);
            
            if (idx != -1)
            {
                ThermVals tval = moprd.getThermoEnergies().get(idx);
                desc.add(tval.getVibEnthalpy());
                desc.add(tval.getVibHeatCap());
                desc.add(tval.getVibEntropy());
                desc.add(tval.getRotEnthalpy());
                desc.add(tval.getRotHeatCap());
                desc.add(tval.getRotEntropy());
                desc.add(tval.getTraEnthalpy());
                desc.add(tval.getTraHeatCap());
                desc.add(tval.getTraEntropy());
                desc.add(tval.getHeatOfFormation());
            }
            else
            {
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
                desc.add(0.);
            }
        }

        names.add(prefix + "POL_ALPHA");
        names.add(prefix + "POL_BETA");
        names.add(prefix + "POL_GAMMA");

        if (moprd.getPolarizabilities().isEmpty())
        {
            desc.add(Double.NaN);
            desc.add(Double.NaN);
            desc.add(Double.NaN);
        }
        else
        {
            for (Double dbl:moprd.getPolarizabilities())
                desc.add(dbl);
        }

        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
    
//------------------------------------------------------------------------------    

    private static double evaluateGroup(String pgrp)
    {
        double val = Double.NaN;
        if (POINTGROUP.containsKey(pgrp))
        {
            val = POINTGROUP.get(pgrp);
        }
        
        return val;
    }
    
//------------------------------------------------------------------------------    

    private static int findClosest(ArrayList<ThermVals> vals, double value)
    {
        int idx = -1;
        double dist = Double.MAX_VALUE, d = 0.;
        for (int i=0; i<vals.size(); i++)
        {
            d = Math.abs(vals.get(i).getTemperature() - value);
            if (d < dist)
            {
                dist = d;
                idx = i;
            }
        }
        return idx;
    }
    

//------------------------------------------------------------------------------

    private static double getMinValue(ArrayList<Double> vals)
    {
        return Collections.min(vals);
    }

//------------------------------------------------------------------------------

    private static double getMaxValue(ArrayList<Double> vals)
    {
        return Collections.max(vals);
    }

//------------------------------------------------------------------------------


}
