package krakenx.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.vecmath.Point3d;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * WHIM descriptors are based on statistical indices calculated on the projections 
 * of atoms along principal axes ($\mathbf{t}_m$). The aim is to capture 3D 
 * information regarding size, shape, symmetry and atom distributions with respect 
 * to invariant reference frames. The algorithm essentially carries out a PCA on 
 * the centered cartesian coordinates of a molecule by using a weighted covariance 
 * matrix.
 */
public class WHIM 
{
    /**
     * Molecular descriptors obtained as statistical indices of the atoms 
     * projected onto the 3 principal components obtained from weighted (charge, 
     * charge density, self polarizability, nucleophilic delocalizability, 
     * electrophilic delocalizability and radical delocalizability) covariance 
     * matrices of the atomic coordinates.
     * @param mol atom container
     * @param lstWts weights to be applied according to atomic property
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     */
    
    public static DescriptorContainer calculateWHIMDescriptors(IAtomContainer mol, 
            HashMap<String, Boolean> lstWts, MOPACReader moprd, String prefix)
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        // for the non-hydrogen atoms only
        int ctr = 0;
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
             if (mol.getAtom(i).getSymbol().equals("H"))
                    continue;
             ctr++;
        }
        
        double[][] cmatNH = new double[ctr][3];
        int j = 0;
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            if (mol.getAtom(i).getSymbol().equals("H"))
                    continue;
            Point3d coords = mol.getAtom(i).getPoint3d();
            cmatNH[j][0] = coords.x;
            cmatNH[j][1] = coords.y;
            cmatNH[j][2] = coords.z;
            j++;
        }
        
        // set up the weight vector
        double[] wtNH = new double[ctr];
        
        
        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("SPOL"))
                {
                    for (j=0; j<moprd.getAtomSelfPolarizabilities().size(); j++)
                        wtNH[j] = moprd.getAtomSelfPolarizabilities().get(j);
                    //System.err.println("spol");
                    computeWHIM(cmatNH, wtNH, "spol", desc, names, prefix);
                }
                
                if (entry.getKey().equals("NDLOC"))
                {
                    for (j=0; j<moprd.getNucleophilicDelocalizabilities().size(); j++)
                        wtNH[j] = moprd.getNucleophilicDelocalizabilities().get(j);
                    //System.err.println("ndloc");
                    computeWHIM(cmatNH, wtNH, "nucdloc", desc, names, prefix);
                }
                
                if (entry.getKey().equals("EDLOC"))
                {
                    for (j=0; j<moprd.getElectrophilicDelocalizabilities().size(); j++)
                        wtNH[j] = moprd.getElectrophilicDelocalizabilities().get(j);
                    //System.err.println("eldloc");
                    computeWHIM(cmatNH, wtNH, "elcdloc", desc, names, prefix);
                }
                
                if (entry.getKey().equals("RDLOC"))
                {
                    double f = 0;
                    for (j=0; j<moprd.getElectrophilicDelocalizabilities().size(); j++)
                    {
                        f = (moprd.getElectrophilicDelocalizabilities().get(j) 
                                + moprd.getNucleophilicDelocalizabilities().get(j));
                        wtNH[j] = f;
                    }
                    //System.err.println("radloc");
                    computeWHIM(cmatNH, wtNH, "radloc", desc, names, prefix);
                }
            }   
        }
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
//------------------------------------------------------------------------------    
    
    /**
     * Calculate the WHIM descriptor for the given set of property weights
     * @param cmat covariance matrix
     * @param wt vector of weights corresponding to the atoms
     * @param wtType property based weighting to be applied
     * @param desc vector of descriptors
     * @param names vector of names
     * @param prefix  prefix descriptor names with specified text
     */
    
    private static void computeWHIM(double[][] cmat, double[] wt, String wtType,
                ArrayList<Double> desc, ArrayList<String> names, String prefix)
    {
        int n = wt.length;
        double sumwt = 0.;
        for (int i = 0; i < n; i++)
        {
            sumwt += wt[i];
        }
        
        names.add(prefix + "Wlambda1_" + wtType);
        names.add(prefix + "Wlambda2_" + wtType);
        names.add(prefix + "Wlambda3_" + wtType);
        names.add(prefix + "Wnu1_" + wtType);
        names.add(prefix + "Wnu2_" + wtType);
        names.add(prefix + "Wgamma1_" + wtType);
        names.add(prefix + "Wgamma2_" + wtType);
        names.add(prefix + "Wgamma3_" + wtType);
        names.add(prefix + "Weta1_" + wtType);
        names.add(prefix + "Weta2_" + wtType);
        names.add(prefix + "Weta3_" + wtType);
        names.add(prefix + "WT_" + wtType);
        names.add(prefix + "WA_" + wtType);
        names.add(prefix + "WV_" + wtType);
        names.add(prefix + "WK_" + wtType);
        names.add(prefix + "WG_" + wtType);
        names.add(prefix + "WD_" + wtType);
        
        if (Math.abs(sumwt) > 0)
        {
            PCA pcaobject = new PCA(cmat, wt);
            double[] lambda = pcaobject.getEigenvalues();        
            double[] gamma = new double[3];
            double[] nu = new double[3];
            double[] eta = new double[3];

            double sum = 0.0;
            for (int i = 0; i < 3; i++)
                sum += lambda[i];
            for (int i = 0; i < 3; i++)
                nu[i] = lambda[i] / sum;

            double[][] scores = pcaobject.getScores();
            for (int i = 0; i < 3; i++)
            {
                sum = 0.0;
                for (int j = 0; j < n; j++)
                    sum += Math.pow(scores[j][i], 4);
                eta[i] = (lambda[i] * lambda[i] * n)/sum;
            }

            for (int i = 0; i < 3; i++)
            {
                double ns = 0.0;
                double na = 0.0;
                for (int j = 0; j < n; j++)
                {
                    boolean foundmatch = false;
                    for (int k = 0; k < n; k++)
                    {
                        if (k == j)
                            continue;

                        if (Math.abs(Utils.round(scores[j][i],3) + Utils.round(scores[k][i],3)) <= 1e-03)
                        {
                            ns++;
                            foundmatch = true;
                            break;
                        }
                    }
                    if (!foundmatch)
                        na++;
                }

                //-1.0 * ((ns / n) * Math.log(ns / n) / Math.log(2.0) + (na / n) * Math.log(1.0 / n) / Math.log(2.0))
                double tmp = 0;
                if (ns == 0)
                {
                    tmp = 1 - ( (na/(double) n) * Utils.log2(1.0/(double) n));
                }
                else if (na == 0)
                {
                    tmp = 1 - ((ns/(double) n) * Utils.log2(ns/(double) n));
                }
                else
                {
                    tmp = 1 - ((ns/(double) n) * Utils.log2(ns/(double) n) + 
                                (na/(double) n) * Utils.log2(1./(double) n));
                }

                gamma[i] = 1/tmp;
            }

            double K = 0.0;
            sum = 0.0;
            for (int i = 0; i < 3; i++) 
                sum += lambda[i];
            double c = (1.0 / 3.0);
            for (int i = 0; i < 3; i++) 
                K += Math.abs((lambda[i] / sum) - c);

            K = K * (3./4.);


            // non directional WHIMS's
            double t = lambda[0] + lambda[1] + lambda[2];
            double a = lambda[0] * lambda[1] + lambda[0] * lambda[2] + lambda[1] * lambda[2];
            double v = t + a + lambda[0] * lambda[1] * lambda[2];
            double d = (eta[0] + eta[1] + eta[2])/3.;
            double g = Math.pow(gamma[0] * gamma[1] * gamma[2], 1.0 / 3.0);

            for (int i = 2; i >= 0; i--) 
                desc.add(lambda[i]);
            

            desc.add(nu[2]);
            desc.add(nu[1]);

            for (int i = 0; i <3; i++) 
                desc.add(gamma[i]);

            for (int i = 0; i <3; i++) 
                desc.add(eta[i]);

            desc.add(t);        
            desc.add(a);        
            desc.add(g);        
            desc.add(K);        
            desc.add(d);        
            desc.add(v);

            pcaobject.cleanup();
            pcaobject = null;
        }
        else 
        {
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
            desc.add(0.);
        }
    }
    
//------------------------------------------------------------------------------'
  
}
