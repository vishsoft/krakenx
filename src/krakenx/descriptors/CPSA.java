package krakenx.descriptors;

import java.util.ArrayList;
import krakenx.DescriptorContainer;
import org.openscience.cdk.geometry.surface.NumericalSurface;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * Charge partial surface area (CPSA) descriptors (Stanton, D.T. and Jurs, P.C., 
 * Development and Use of Charged Partial Surface Area Structural Descriptors 
 * in Computer Assisted Quantitative Structure Property Relationship Studies, 
 * Anal. Chem., 1990, 62:2323-2329]) along with additional indices proposed 
 * by Aptula et al., 2003.
 * 
 * @author VISHWESH
 */

public class CPSA 
{
    
    /**
     * Calculate CPSA descriptors
     * @param mol Atom container
     * @param solventRad
     * @param prefix prefix descriptor names with specified text
     * @return vector of CPSA indices
     * @throws Exception 
     */
    public static DescriptorContainer 
        getCPSADescripors(IAtomContainer mol, double solventRad, String prefix) throws Exception
    {
        NumericalSurface surface = null;
        try
        {
            int tessLevel = 4;
            surface = new NumericalSurface(mol, solventRad, tessLevel);
            surface.calculateSurface();
        }
        catch (NullPointerException npe)
        {
            throw npe;
        }

        double[] atomSurfaces = surface.getAllSurfaceAreas();
        double totalSA = surface.getTotalSurfaceArea();

        double ppsa1 = 0.0;
        double ppsa3 = 0.0;
        double pnsa1 = 0.0;
        double pnsa3 = 0.0;
        double totpcharge = 0.0;
        double totncharge = 0.0;
        double f = 0, maxposchg = Double.MIN_VALUE, minnegchg = Double.MAX_VALUE;
        double amax = 0, amin = 0;
        int nposatm = 0, nnegatm = 0;
        
        
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            IAtom atm = mol.getAtom(i);
            f = atm.getCharge();
            if (f > 0)
            {
                ppsa1 += atomSurfaces[i];
                ppsa3 += f * atomSurfaces[i];
                totpcharge += f;
                nposatm += 1;
                if (f > maxposchg) {
                    maxposchg = f;
                    amax = atomSurfaces[i];
                }
            }
            else
            {
                pnsa1 += atomSurfaces[i];
                pnsa3 += f * atomSurfaces[i];
                totncharge += f;
                nnegatm += 1;
                if (f < minnegchg) {
                    minnegchg = f;
                    amin = atomSurfaces[i];
                }
            }
        }
        
        
        double ppsa2 = ppsa1 * totpcharge;
        double pnsa2 = pnsa1 * totncharge;
        
        // Aptula et al., 2003
        double ppsa4 = (totpcharge/mol.getAtomCount()) * ppsa1;
        double ppsa5 = (totpcharge/nposatm) * ppsa1;
        double pnsa4 = (totncharge/mol.getAtomCount()) * pnsa1;
        double pnsa5 = (totncharge/nnegatm) * pnsa1;
        double spmx = maxposchg * amax;
        double snmx = minnegchg * amin;
        

        // fractional +ve & -ve SA
        double fpsa1 = ppsa1 / totalSA;
        double fpsa2 = ppsa2 / totalSA;
        double fpsa3 = ppsa3 / totalSA;
        double fnsa1 = pnsa1 / totalSA;
        double fnsa2 = pnsa2 / totalSA;
        double fnsa3 = pnsa3 / totalSA;

        // surface wtd +ve & -ve SA
        double wpsa1 = ppsa1 * totalSA / 1000;
        double wpsa2 = ppsa2 * totalSA / 1000;
        double wpsa3 = ppsa3 * totalSA / 1000;
        double wnsa1 = pnsa1 * totalSA / 1000;
        double wnsa2 = pnsa2 * totalSA / 1000;
        double wnsa3 = pnsa3 * totalSA / 1000;

         // hydrophobic and polar surface area
//        double phobic = 0.0;
//        double polar = 0.0;
//        for (int i = 0; i < mol.getAtomCount(); i++)
//        {
//            IAtom atm = mol.getAtom(i);
//            f = atm.getCharge();
//            if (Math.abs(f) < 0.2)
//                phobic += atomSurfaces[i];
//            else
//                polar += atomSurfaces[i];
//        }
//
//        double thsa = phobic;
//        double tpsa = polar;
//        double rhsa = phobic / totalSA;
//        double rpsa = polar / totalSA;

        // differential +ve & -ve SA
        double dpsa1 = ppsa1 - pnsa1;
        double dpsa2 = ppsa2 - pnsa2;
        double dpsa3 = ppsa3 - pnsa3;

        double maxpcharge = 0.0;
        double maxncharge = 0.0;
        int pidx = 0;
        int nidx = 0;

        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            IAtom atm = mol.getAtom(i);
            f = atm.getCharge();
            if (f > maxpcharge) {
                maxpcharge = f;
                pidx = i;
            }
            if (f < maxncharge) {
                maxncharge = f;
                nidx = i;
            }
        }

        // relative descriptors
        double rpcg = maxpcharge / totpcharge;
        double rncg = maxncharge / totncharge;
        double rpcs = atomSurfaces[pidx] * rpcg;
        double rncs = atomSurfaces[nidx] * rncg;
        
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        names.add(prefix + "PPSA-1"); //	partial positive surface area -- sum of surface area on positive parts of molecule
        names.add(prefix + "PPSA-2"); //	partial positive surface area * total positive charge on the molecule
        names.add(prefix + "PPSA-3"); //	charge weighted partial positive surface area
        names.add(prefix + "PNSA-1"); //	partial negative surface area -- sum of surface area on negative parts of molecule
        names.add(prefix + "PNSA-2"); //	partial negative surface area * total negative charge on the molecule
        names.add(prefix + "PNSA-3"); //	charge weighted partial negative surface area
        names.add(prefix + "DPSA-1"); //	difference of PPSA-1 and PNSA-1
        names.add(prefix + "DPSA-2"); //	difference of FPSA-2 and PNSA-2
        names.add(prefix + "DPSA-3"); //	difference of PPSA-3 and PNSA-3
        names.add(prefix + "FPSA-1"); //	PPSA-1 / total molecular surface area
        names.add(prefix + "FFSA-2"); //	PPSA-2 / total molecular surface area
        names.add(prefix + "FPSA-3"); //	PPSA-3 / total molecular surface area
        names.add(prefix + "FNSA-1"); //	PNSA-1 / total molecular surface area
        names.add(prefix + "FNSA-2"); //	PNSA-2 / total molecular surface area
        names.add(prefix + "FNSA-3"); //	PNSA-3 / total molecular surface area
        names.add(prefix + "WPSA-1"); //	PPSA-1 * total molecular surface area / 1000
        names.add(prefix + "WPSA-2"); //	PPSA-2 * total molecular surface area /1000
        names.add(prefix + "WPSA-3"); //	PPSA-3 * total molecular surface area / 1000
        names.add(prefix + "WNSA-1"); //	PNSA-1 * total molecular surface area /1000
        names.add(prefix + "WNSA-2"); //	PNSA-2 * total molecular surface area / 1000
        names.add(prefix + "WNSA-3"); //	PNSA-3 * total molecular surface area / 1000
        names.add(prefix + "RPCG"); // relative positive charge -- most positive charge / total positive charge
        names.add(prefix + "RNCG"); // relative negative charge -- most negative charge / total negative charge
        names.add(prefix + "RPCS"); // relative positive charge surface area -- most positive surface area * RPCG
        names.add(prefix + "RNCS"); // relative negative charge surface area -- most negative surface area * RNCG
//        names.add("THSA"); // sum of solvent accessible surface areas of atoms with absolute value of partial charges less than 0.2
//        names.add("TPSA"); // sum of solvent accessible surface areas of atoms with absolute value of partial charges greater than or equal 0.2
//        names.add("RHSA"); // THSA / total molecular surface area
//        names.add("RPSA"); // TPSA / total molecular surface area 
        
        names.add(prefix + "PPSA-4"); // Aptula et al., 2003
        names.add(prefix + "PPSA-5");
        names.add(prefix + "PNSA-4");
        names.add(prefix + "PNSA-5");
        names.add(prefix + "SPMX");
        names.add(prefix + "SNMX");
        
        desc.add(ppsa1);
        desc.add(ppsa2);
        desc.add(ppsa3);
        desc.add(pnsa1);
        desc.add(pnsa2);
        desc.add(pnsa3);
        desc.add(dpsa1);
        desc.add(dpsa2);
        desc.add(dpsa3);
        desc.add(fpsa1);
        desc.add(fpsa2);
        desc.add(fpsa3);
        desc.add(fnsa1);
        desc.add(fnsa2);
        desc.add(fnsa3);
        desc.add(wpsa1);
        desc.add(wpsa2);
        desc.add(wpsa3);
        desc.add(wnsa1);
        desc.add(wnsa2);
        desc.add(wnsa3);
        desc.add(rpcg);
        desc.add(rncg);
        desc.add(rpcs);
        desc.add(rncs);
//        desc.add(thsa);
//        desc.add(tpsa);
//        desc.add(rhsa);
//        desc.add(rpsa);
        desc.add(ppsa4);
        desc.add(ppsa5);
        desc.add(pnsa4);
        desc.add(pnsa5);
        desc.add(spmx);
        desc.add(snmx);
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
//------------------------------------------------------------------------------    
    
}
