package krakenx.descriptors;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.KrakenException;
import krakenx.MOPACReader;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 *
 * @author vishwesv
 */
public class GraphEnergy
{
    public static DescriptorContainer calculateEigenDescriptors(IAtomContainer mol,
        HashMap<String, Boolean> lstWts, MOPACReader moprd, String prefix) throws KrakenException
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();


        int n = mol.getAtomCount();

        if (n == 1)
        {
            return handleSingleAtomCase(mol, lstWts, prefix, moprd, desc, names);
        }

        double[][] matrix = new double[n][n];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                matrix[i][j] = 0.0;
            }
        }

        int k = 0;

        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    for (int i = 0; i < n; i++)
                    {
                        matrix[i][i] = mol.getAtom(i).getCharge();
                    }

                    computeGEValue(matrix, "chg", desc, names, prefix);
                }

                if (entry.getKey().equals("SPOL"))
                {
                    k = 0;
                    for (int i = 0; i < n; i++)
                    {
                        if (mol.getAtom(i).getSymbol().equals("H"))
                            continue;
                        matrix[i][i] = moprd.getAtomSelfPolarizabilities().get(k);
                        k++;
                    }

                    computeGEValue(matrix, "spol", desc, names, prefix);
                }

                if (entry.getKey().equals("NDLOC"))
                {
                    k = 0;
                    for (int i = 0; i < n; i++)
                    {
                        if (mol.getAtom(i).getSymbol().equals("H"))
                            continue;
                        matrix[i][i] = moprd.getNucleophilicDelocalizabilities().get(k);
                        k++;
                    }

                    computeGEValue(matrix, "nucdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("EDLOC"))
                {
                    k = 0;
                    for (int i = 0; i < n; i++)
                    {
                        if (mol.getAtom(i).getSymbol().equals("H"))
                            continue;
                        matrix[i][i] = moprd.getElectrophilicDelocalizabilities().get(k);
                        k++;
                    }

                    computeGEValue(matrix, "elcdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("RDLOC"))
                {
                    double f;
                    k = 0;
                    for (int i = 0; i < n; i++)
                    {
                        if (mol.getAtom(i).getSymbol().equals("H"))
                            continue;
                        f = (moprd.getNucleophilicDelocalizabilities().get(k)
                                + moprd.getElectrophilicDelocalizabilities().get(k));

                        matrix[i][i] = f;
                        k++;
                    }
                    computeGEValue(matrix, "radloc", desc, names, prefix);
                }
            }
        }


        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    private static DescriptorContainer handleSingleAtomCase(IAtomContainer mol,
        HashMap<String, Boolean> lstWts, String prefix, MOPACReader moprd,
        ArrayList<Double> desc, ArrayList<String> names)
    {
        DescriptorContainer dc = new DescriptorContainer();

        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    names.add(prefix + "GE_" + "chg");
                    desc.add(Math.abs(mol.getAtom(0).getCharge()));
                    
                    names.add(prefix + "EE_" + "chg");
                    desc.add(Math.exp(mol.getAtom(0).getCharge()));
                }
                if (entry.getKey().equals("SPOL"))
                {
                    names.add(prefix + "GE_" + "spol");
                    names.add(prefix + "EE_" + "spol");
                    if (mol.getAtom(0).getSymbol().equals("H"))
                    {
                        desc.add(0.);
                        desc.add(1.);
                    }
                    else
                    {
                        desc.add(Math.abs(moprd.getAtomSelfPolarizabilities().get(0)));
                        desc.add(Math.exp(moprd.getAtomSelfPolarizabilities().get(0)));
                    }
                }
                if (entry.getKey().equals("RDLOC"))
                {
                    names.add(prefix + "GE_" + "radloc");
                    names.add(prefix + "EE_" + "radloc");
                    if (mol.getAtom(0).getSymbol().equals("H"))
                    {
                        desc.add(0.);
                        desc.add(1.);
                    }
                    else
                    {
                        double f = (moprd.getNucleophilicDelocalizabilities().get(0)
                                + moprd.getElectrophilicDelocalizabilities().get(0));
                        desc.add(Math.abs(f));
                        desc.add(Math.exp(f));
                    }
                }
                if (entry.getKey().equals("EDLOC"))
                {
                    names.add(prefix + "GE_" + "elcdloc");
                    names.add(prefix + "EE_" + "elcdloc");
                    if (mol.getAtom(0).getSymbol().equals("H"))
                    {
                        desc.add(0.);
                        desc.add(1.);
                    }
                    else
                    {
                        desc.add(Math.abs(moprd.getElectrophilicDelocalizabilities().get(0)));
                        desc.add(Math.abs(moprd.getElectrophilicDelocalizabilities().get(0)));
                    }
                }
                if (entry.getKey().equals("NDLOC"))
                {
                    names.add(prefix + "GE_" + "nucdloc");
                    names.add(prefix + "EE_" + "nucdloc");
                    if (mol.getAtom(0).getSymbol().equals("H"))
                    {
                        desc.add(0.);
                        desc.add(1.);
                    }
                    else
                    {
                        desc.add(Math.abs(moprd.getNucleophilicDelocalizabilities().get(0)));
                        desc.add(Math.exp(moprd.getNucleophilicDelocalizabilities().get(0)));
                    }
                }
            }
        }

        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    private static void computeGEValue(double[][] cmat, String wtType,
            ArrayList<Double> desc, ArrayList<String> names, String prefix)
    {
        Matrix bmtx = new Matrix(cmat);
        EigenvalueDecomposition eigenDecomposition = new EigenvalueDecomposition(bmtx);
        double[] eval = eigenDecomposition.getRealEigenvalues();


        // Bulletin, Classe des Sciences Mathématiques et Naturelles, Sciences mathématiques naturelles / sciences mathematiques
        // Vol. CXLIV, No. 37, pp. 1–17 (2012)
        double val = 0;
        for (int i=0; i<eval.length; i++)
        {
            val += Math.abs(eval[i]);
        }

        names.add(prefix + "GE_" + wtType);
        desc.add(val);


        // CROATICA CHEMICA ACTA, 80 (2) 151-154 (2007)
        val = 0;
        for (int i=0; i<eval.length; i++)
        {
            val += Math.exp(eval[i]);
        }

        names.add(prefix + "EE_" + wtType);
        desc.add(val);
    }

//------------------------------------------------------------------------------

}
