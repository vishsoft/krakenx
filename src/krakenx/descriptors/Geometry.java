package krakenx.descriptors;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.ArrayList;
import javax.vecmath.Point3d;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.geometry.surface.NumericalSurface;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.geometry.volume.VABCVolume;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;

/**
 *
 * @author VISHWESH
 */
public class Geometry 
{
    /**
     * Calculate geometry descriptors (3D Wiener,radius of gyration,Inertial 
     * shape factor,molecular eccentricity,asphericity,spherosity,globularity,
     * ovality) based on the optimized geometries. See Geometrical Descriptors 
     * for Molecular Size and Shape, Todeschini, R and Consonni, V.
     * 
     * @param mol Atom container
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     * @throws CDKException 
     */
    
    public static DescriptorContainer getGeometryDescriptors(IAtomContainer mol,
            MOPACReader moprd, String prefix) throws CDKException
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        double Ia = moprd.getPIMX();
        double Ib = moprd.getPIMY();
        double Ic = moprd.getPIMZ();
        double mw = moprd.getMW();
        double r_ij;
        
        // calculate geometric distance matrix
        double[][] gmat = new double[mol.getAtomCount()][mol.getAtomCount()];
        double sum = 0;
        for (int i = 0; i < mol.getAtomCount()-1; i++)
        {
            for (int j = i+1; j < mol.getAtomCount(); j++)
            {
                r_ij = Utils.distance(mol.getAtom(i).getPoint3d(), mol.getAtom(j).getPoint3d());
                gmat[i][j] = r_ij;
                gmat[j][i] = r_ij;
            }
        }
        
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            for (int j = 0; j < mol.getAtomCount(); j++)
            {
                sum += gmat[i][j];
            }
        }
        
        desc.add(sum/2.); // 3D Wiener
        
        // radius of gyration
        if (Math.abs(Ic) <= 1e-04)
        {
            desc.add(Math.sqrt(Math.sqrt(Ia*Ib)/mw));
        }
        else
        {
            desc.add(Math.sqrt(Math.pow(Ia*Ib*Ic, 1./3.) * 2 * Math.PI/mw));
        }
        
        
        // Inertial shape factor
        desc.add(Ib/(Ia * Ic)); // cannot be calculated for planar molecules
        
        // molecular eccentricity
        desc.add(Math.sqrt(Ic*Ic - Ia*Ia)/Ic); // 0 <= e <= 1
        
        // Asphericity
        double f = 0.5 * (Math.pow(Ia-Ib, 2) + Math.pow(Ia-Ic, 2) + Math.pow(Ib-Ic, 2))
                        /(Ia*Ia + Ib*Ib + Ic*Ic);
        desc.add(f);
        
        
        // spherosity
        double[][] cmat = new double[mol.getAtomCount()][3];
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            Point3d coords = mol.getAtom(i).getPoint3d();
            cmat[i][0] = coords.x;
            cmat[i][1] = coords.y;
            cmat[i][2] = coords.z;
        }
        
        Matrix bmtx = getCovarianceMatrix(cmat);
        
        if (!containsNaN(bmtx))
        {
        
            EigenvalueDecomposition eigenDecomposition = new EigenvalueDecomposition(bmtx);
            double[] eval = eigenDecomposition.getRealEigenvalues();

            f = 3 * eval[2]/(eval[0] + eval[1] + eval[2]);
            desc.add(f);
            
            desc.add(eval[0]/eval[2]); // Globularity
        }
        else
        {
            desc.add(0.);
            desc.add(0.);
        }
        
        
        // ovality
        try
        {
            AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);
            double vabcvol = VABCVolume.calculate(mol);
            NumericalSurface surface = new NumericalSurface(mol, 0.0, 4); // van der Waals
            surface.calculateSurface();
            double totalSA = surface.getTotalSurfaceArea();
            double ovality = totalSA/(4 * Math.PI * Math.pow(3 * vabcvol/(4 * Math.PI), 2./3.));
            desc.add(ovality);
        }
        catch (CDKException cdke)
        {
            System.err.println(cdke.getMessage());
            desc.add(Double.NaN);
        }
        
        
        
        
        names.add(prefix + "Wiener3D"); // 3D Wiener
        names.add(prefix + "RadGyration"); // radius of gyration
        names.add(prefix + "InertialSF"); // inertial shape factor
        names.add(prefix + "MolEccentricity"); // Molecular eccentricity         
        names.add(prefix + "Asphericity"); // asphericity
        names.add(prefix + "Spherosity");
        names.add(prefix + "Globularity");
        names.add(prefix + "Ovality");
        
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
    
//------------------------------------------------------------------------------
    
    private static boolean containsNaN(Matrix mtx)
    {
        for (int i=0; i<mtx.getRowDimension(); i++)
        {
            for (int j=0; j<mtx.getColumnDimension(); j++)
            {
                if (Double.isNaN(mtx.get(i, j)))
                    return true;
                if (Double.isInfinite(mtx.get(i, j)))
                    return true;
            }
        }
        
        
        return false;
    }
    
//------------------------------------------------------------------------------
    
    private static Matrix getCovarianceMatrix(double[][] mtx)
    {
        // subtract column-averages
        double average = 0;
        
        int numberOfColumns = mtx[0].length;
        for (int c = 0; c < numberOfColumns; c++) 
        {
            average = getAverageForColumn(mtx, c);
            for (int r = 0; r < mtx.length; r++) 
            {
                mtx[r][c] -= average;
            }
        }
 
        // create covariance matrix
        double[][] covarianceMatrixEntries = new double[numberOfColumns][numberOfColumns];
 
        // fill the covariance matrix
        for (int i = 0; i < covarianceMatrixEntries.length; i++) 
        {
            for (int j = i; j < covarianceMatrixEntries.length; j++) 
            {
                double covariance = getCovariance(mtx, i, j);
                covarianceMatrixEntries[i][j] = covariance;
                covarianceMatrixEntries[j][i] = covariance;
            }
        }
 
        return new Matrix(covarianceMatrixEntries);
    }
    
//------------------------------------------------------------------------------
    
    private static double getAverageForColumn(double[][] data, int column) 
    {
        double sum = 0.0d;
        for (int r = 0; r < data.length; r++) 
        {
            sum += data[r][column];
        }
        return sum / data.length;
    }
    
//------------------------------------------------------------------------------    
 
    private static double getCovariance(double[][] data, int x, int y) 
    {
        double cov = 0;
        for (int i = 0; i < data.length; i++) 
        {
            cov += data[i][x] * data[i][y];
        }
        return cov / (data.length - 1);
    }
    
//------------------------------------------------------------------------------    
}
