package krakenx.descriptors;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.KrakenException;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * 3D BCUT indices obtained from the positive and negative eigenvalues of 
 * the atom distance matrix, weighting the diagonal elements with atom weights. 
 * See Burden. F.R., J.Chem. inf. Comput. Sci.,  1989, 29, 225-227.
 * @author VISHWESH
 */
public class BCUT
{
    /**
     * 3D BCUT indices (excluding hydrogen atoms) weighted by charge, 
     * charge density, self polarizability, nucleophilic delocalizability, 
     * electrophilic delocalizability and radical delocalizability
     * @param mol atom container
     * @param lstWts weights to be applied according to atomic property
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     * @throws KrakenException 
     */
    
    public static DescriptorContainer calculateBCUTDescriptors(IAtomContainer mol,
        HashMap<String, Boolean> lstWts, MOPACReader moprd, String prefix) throws KrakenException
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        // find number of heavy atoms
        int nheavy = 0;
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            if (!mol.getAtom(i).getSymbol().equals("H"))
                nheavy++;
        }

        if (nheavy == 0)
            throw new KrakenException("No heavy atoms in the molecule");

        double[][] matrix = new double[nheavy][nheavy];
        for (int i = 0; i < nheavy; i++)
        {
            for (int j = 0; j < nheavy; j++)
            {
                matrix[i][j] = 0.0;
            }
        }

        ArrayList<Integer> lstHAtm = new ArrayList<>();
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
             if (mol.getAtom(i).getSymbol().equals("H"))
                    continue;
             lstHAtm.add(i);
        }

        for (int i = 0; i < lstHAtm.size() - 1; i++)
        {
            IAtom A = mol.getAtom(lstHAtm.get(i));
            for (int j = i + 1; j < lstHAtm.size(); j++)
            {
                IAtom B = mol.getAtom(lstHAtm.get(j));
                matrix[i][j] = Utils.distance(A.getPoint3d(), B.getPoint3d());
                matrix[j][i] = matrix[i][j];
            }
        }

        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    for (int i = 0; i < lstHAtm.size(); i++)
                    {
                        matrix[i][i] = mol.getAtom(lstHAtm.get(i)).getCharge();
                    }
                    
                    computeBCUT(matrix, "chg", desc, names, prefix);
                }

                if (entry.getKey().equals("SPOL"))
                {
                    for (int i = 0; i < moprd.getAtomSelfPolarizabilities().size(); i++)
                    {
                        matrix[i][i] = moprd.getAtomSelfPolarizabilities().get(i);
                    }
                    
                    computeBCUT(matrix, "spol", desc, names, prefix);
                }
                
                if (entry.getKey().equals("CDEN"))
                {
                    for (int i = 0; i < lstHAtm.size(); i++)
                    {
                        matrix[i][i] = moprd.getAtomChargeDensities().get(lstHAtm.get(i));
                    }
                    
                    computeBCUT(matrix, "cden", desc, names, prefix);
                }

                if (entry.getKey().equals("NDLOC"))
                {
                    for (int i = 0; i < moprd.getNucleophilicDelocalizabilities().size(); i++)
                    {
                        matrix[i][i] = moprd.getNucleophilicDelocalizabilities().get(i);
                    }
                    
                    computeBCUT(matrix, "nucdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("EDLOC"))
                {
                    for (int i = 0; i < moprd.getElectrophilicDelocalizabilities().size(); i++)
                    {
                        matrix[i][i] = moprd.getElectrophilicDelocalizabilities().get(i);
                    }
                    computeBCUT(matrix, "elcdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("RDLOC"))
                {
                    double f;
                    for (int i=0; i<moprd.getElectrophilicDelocalizabilities().size(); i++)
                    {
                        f = (moprd.getElectrophilicDelocalizabilities().get(i)
                                + moprd.getNucleophilicDelocalizabilities().get(i));
                        matrix[i][i] = f;
                    }
                    computeBCUT(matrix, "radloc", desc, names, prefix);
                }
            }
        }


        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    /**
     * Calculate the BCUT descriptor for the given set of property weights
     * @param cmat weighted distance matrix
     * @param wtType property based weighting to be applied
     * @param desc vector of descriptors
     * @param names vector of names
     * @param prefix  prefix descriptor names with specified text 
     */
    
    private static void computeBCUT(double[][] cmat, String wtType,
            ArrayList<Double> desc, ArrayList<String> names, String prefix)
    {
        Matrix bmtx = new Matrix(cmat);
        EigenvalueDecomposition eigenDecomposition = new EigenvalueDecomposition(bmtx);
        double[] eval = eigenDecomposition.getRealEigenvalues();
        
        int nhigh = 1, nlow = 1;
        for (int i = 0; i < nlow; i++)
            desc.add(eval[i]);
         for (int i = 0; i < nhigh; i++) 
            desc.add(eval[eval.length-i-1]);

        names.add(prefix + "BEL_" + wtType);
        names.add(prefix + "BEH_" + wtType);
    }


//------------------------------------------------------------------------------
}
