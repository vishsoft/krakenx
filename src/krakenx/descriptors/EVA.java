package krakenx.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 *
 * @author VISHWESH
 */
public class EVA
{
    /**
     *
     * @param pars
     * @param vals
     * @param prefix
     * @return
     */

    public static DescriptorContainer calculateEVA(HashMap<String, ArrayList<Double>> pars,
                ArrayList<Double> vals, String prefix)
    {
        // split BFS
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        double minVal = pars.get("MINVAL").get(0);
        double maxVal = pars.get("MAXVAL").get(0);

        ArrayList<Double> lstLVals = pars.get("L");
        ArrayList<Double> lstSVals = pars.get("SIGMA");
        double f = 0;

        for (double curL:lstLVals)
        {
            for (double curS:lstSVals)
            {
                for (double x=minVal; x<=maxVal; x+=curL)
                {
                    names.add(prefix + "_" + curS + "_" + curL + "_" + String.format("%.3f", x) + " ");
                    if (!vals.isEmpty())
                    {
                        f = getValue(curS, vals, x, minVal, maxVal, false);                    
                        desc.add(Utils.round(f, 3));
                    }
                    else
                    {
                        desc.add(0.);
                    }
                }
            }
        }

        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @param mol
     * @param lstCEVAPars
     * @param fftype
     * @param prefix
     * @return
     * @throws Exception 
     */
    
    public static DescriptorContainer getCEVADescriptors(IAtomContainer mol,
        HashMap<String, ArrayList<Double>> lstCEVAPars, String fftype, 
        String prefix) throws Exception
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        Iterator<Map.Entry<String, ArrayList<Double>>> iterator =
                                        lstCEVAPars.entrySet().iterator();

        
        boolean parse = Utils.percieveAtomTypes(mol, fftype);
        if (!parse)
        {
            System.err.println("Molecule may have unidentified atom types.");
        }
        
        String atype, atype_C;
        double val, minVal, maxVal, sigma, L;

        while (iterator.hasNext())
        {
            Map.Entry<String, ArrayList<Double>> entry = iterator.next();
            atype_C = entry.getKey();
            ArrayList<Double> pars = entry.getValue();
            sigma = pars.get(0);
            L = pars.get(1);
            minVal = pars.get(2);
            maxVal = pars.get(3);
            
            
            // collect atoms of the same type
            ArrayList<Double> X = new ArrayList<>();
            for (int j = 0; j < mol.getAtomCount(); j++)
            {
                val = mol.getAtom(j).getCharge();
                atype = "AT_" + mol.getAtom(j).getAtomTypeName();
                if (atype == null)
                    continue;
                if (atype.equalsIgnoreCase(atype_C))
                {
                    X.add(val);
                }
            }
            
            if (!X.isEmpty())
            {
                for (double g=minVal; g<=maxVal; g+=L)
                {
                   double f = getValue(sigma, X, g, minVal, maxVal, false);
                   desc.add(f);
                   names.add(prefix + "_" + atype_C + "_" + String.format("%.3f", sigma) + "_" + String.format("%.3f", L) + "_" + String.format("%.3f", g) + " ");
                }
            }
            else 
            {
                for (double g=minVal; g<=maxVal; g+=L)
                {
                    desc.add(0.0);
                    names.add(prefix + "_" + atype_C + "_" + String.format("%.3f", sigma) + "_" + String.format("%.3f", L) + "_" + String.format("%.3f", g) + " ");
                }
            }

        }


        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }


//------------------------------------------------------------------------------

    /**
     *
     * @param sigma
     * @param vals
     * @param xval
     * @param minVal
     * @param maxVal
     * @param lorentzian
     * @return
     */

    private static double getValue(double sigma, ArrayList<Double> vals, double xval,
            double minVal, double maxVal, boolean lorentzian)
    {
        double sum = 0;

        if (!lorentzian)
        {
            double s1 = 1.0/(2.0*sigma*sigma);
            double s0 = 1.0/sigma;

            for (int i=0; i<vals.size(); i++)
            {
                double f = vals.get(i);
                if (f > maxVal || f < minVal)
                    continue;
                double s2 = (xval - f)*(xval - f);

                sum += Utils.SQRTINVPI * s0 * Math.exp(-s2*s1);
            }
        }
        else
        {
            for (int i=0; i<vals.size(); i++)
            {
                // f if the location parameter specifying the location of the peak
                // of the distribution
                double f = vals.get(i);
                if (f > maxVal || f < minVal)
                    continue;
                double s2 = (xval - f)*(xval - f);
                double s1 = sigma * 0.5;

                // sigma larger the scale/wdith parameter, the more spread out
                // the distribution
                sum += Utils.INVPI * s1 * (1.0/(s2 + s1*s1));
            }
        }

        return sum;
    }

//------------------------------------------------------------------------------
}
