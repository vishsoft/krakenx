package krakenx.descriptors;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.KrakenException;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 *
 * @author vishwesv
 */
public class CoulombMatrix
{
    private static final Map<String, Double> NCMAP = createMap();

    private static Map<String, Double> createMap() 
    {
        Map<String, Double> ncmap = new HashMap<>();
        ncmap.put("H",1.0);
        ncmap.put("He",2.0);
        ncmap.put("Li",3.0);
        ncmap.put("Be",4.0);
        ncmap.put("B",5.0);
        ncmap.put("C",6.0);
        ncmap.put("N",7.0);
        ncmap.put("O",8.0);
        ncmap.put("F",9.0);
        ncmap.put("Ne",10.0);
        ncmap.put("Na",11.0);
        ncmap.put("Mg",12.0);
        ncmap.put("Al",13.0);
        ncmap.put("Si",14.0);
        ncmap.put("P",15.0);
        ncmap.put("S",16.0);
        ncmap.put("Cl",17.0);
        ncmap.put("Ar",18.0);
        ncmap.put("K",19.0);
        ncmap.put("Ca",20.0);
        ncmap.put("Sc",21.0);
        ncmap.put("Ti",22.0);
        ncmap.put("V",23.0);
        ncmap.put("Cr",24.0);
        ncmap.put("Mn",25.0);
        ncmap.put("Fe",26.0);
        ncmap.put("Co",27.0);
        ncmap.put("Ni",28.0);
        ncmap.put("Cu",29.0);
        ncmap.put("Zn",30.0);
        ncmap.put("Ga",31.0);
        ncmap.put("Ge",32.0);
        ncmap.put("As",33.0);
        ncmap.put("Se",34.0);
        ncmap.put("Br",35.0);
        ncmap.put("Kr",36.0);
        ncmap.put("Rb",37.0);
        ncmap.put("Sr",38.0);
        ncmap.put("Y",39.0);
        ncmap.put("Zr",40.0);
        ncmap.put("Nb",41.0);
        ncmap.put("Mo",42.0);
        ncmap.put("Tc",43.0);
        ncmap.put("Ru",44.0);
        ncmap.put("Rh",45.0);
        ncmap.put("Pd",46.0);
        ncmap.put("Ag",47.0);
        ncmap.put("Cd",48.0);
        ncmap.put("In",49.0);
        ncmap.put("Sn",50.0);
        ncmap.put("Sb",51.0);
        ncmap.put("Te",52.0);
        ncmap.put("I",53.0);
        ncmap.put("Xe",54.0);
        ncmap.put("Cs",55.0);
        ncmap.put("Ba",56.0);
        ncmap.put("La",57.0);
        ncmap.put("Ce",58.0);
        ncmap.put("Pr",59.0);
        ncmap.put("Nd",60.0);
        ncmap.put("Pm",61.0);
        ncmap.put("Sm",62.0);
        ncmap.put("Eu",63.0);
        ncmap.put("Gd",64.0);
        ncmap.put("Tb",65.0);
        ncmap.put("Dy",66.0);
        ncmap.put("Ho",67.0);
        ncmap.put("Er",68.0);
        ncmap.put("Tm",69.0);
        ncmap.put("Yb",70.0);
        ncmap.put("Lu",71.0);
        ncmap.put("Hf",72.0);
        ncmap.put("Ta",73.0);
        ncmap.put("W",74.0);
        ncmap.put("Re",75.0);
        ncmap.put("Os",76.0);
        ncmap.put("Ir",77.0);
        ncmap.put("Pt",78.0);
        ncmap.put("Au",79.0);
        ncmap.put("Hg",80.0);
        ncmap.put("Tl",81.0);
        ncmap.put("Pb",82.0);
        ncmap.put("Bi",83.0);
        ncmap.put("Po",84.0);
        ncmap.put("At",85.0);
        ncmap.put("Rn",86.0);
        ncmap.put("Fr",87.0);
        ncmap.put("Ra",88.0);
        ncmap.put("Ac",89.0);
        ncmap.put("Th",90.0);
        ncmap.put("Pa",91.0);
        ncmap.put("U",92.0);
        ncmap.put("Np",93.0);
        ncmap.put("Pu",94.0);
        ncmap.put("Am",95.0);
        ncmap.put("Cm",96.0);
        ncmap.put("Bk",97.0);
        ncmap.put("Cf",98.0);
        ncmap.put("Es",99.0);
        ncmap.put("Fm",100.0);
        ncmap.put("Md",101.0);
        ncmap.put("No",102.0);
        ncmap.put("Lr",103.0);
        ncmap.put("Rf",104.0);
        ncmap.put("Db",105.0);
        ncmap.put("Sg",106.0);
        ncmap.put("Bh",107.0);
        ncmap.put("Hs",108.0);
        ncmap.put("Mt",109.0);
        ncmap.put("Ds",110.0);
        ncmap.put("Rg",111.0);
        ncmap.put("Uub",112.0);
        ncmap.put("Uut",113.0);
        ncmap.put("Uuq",114.0);
        ncmap.put("Uup",115.0);
        ncmap.put("Uuh",116.0);
        ncmap.put("Uus",117.0);
        ncmap.put("Uuo",118.0);

        return Collections.unmodifiableMap(ncmap);
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @param mol
     * @param maxlen
     * @param prefix
     * @return
     * @throws KrakenException 
     */
    
    public static DescriptorContainer calculateCoulombMatrix(IAtomContainer mol,
        int maxlen, String prefix) throws KrakenException
    {
        // split BFS
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        double r_ij = 0;

        int n = mol.getAtomCount();
        double[][] matrix = new double[n][n];
        for (int i = 0; i < n; i++)
        {
            double achg = getNuclearCharge(mol.getAtom(i).getSymbol());
            matrix[i][i] = 0.5 * Math.pow(achg, 2.4);
        }
        
        for (int i = 0; i < n-1; i++)
        {
            double achg = getNuclearCharge(mol.getAtom(i).getSymbol());
            for (int j = i+1; j < n; j++)
            {
                double bchg = getNuclearCharge(mol.getAtom(j).getSymbol());
                r_ij = Utils.distance(mol.getAtom(i).getPoint3d(), mol.getAtom(j).getPoint3d());
                matrix[i][j] = (achg * bchg)/r_ij;
                matrix[j][i] = (achg * bchg)/r_ij;
            }
        }

        Matrix bmtx = new Matrix(matrix);
        EigenvalueDecomposition eigenDecomposition = new EigenvalueDecomposition(bmtx);
        double[] eval = eigenDecomposition.getRealEigenvalues();
        
        //for (int i=0; i<eval.length; i++)
        //    System.err.print(eval[i] + " ");
        //System.err.println();

        int k = eval.length-1;
        for (int i=0;i<maxlen; i++)
        {
            if (i < eval.length)
            {
                desc.add(eval[k]);
                //System.err.println(k + " " + eval[k] + " " + i);
                k = k - 1;
            }
            else
                desc.add(0.);
        }

        int ndigits = String.valueOf(maxlen).length();
        for (int i=0; i<maxlen; i++)
        {
            names.add(prefix + "E" + Utils.getPaddedString(ndigits, i+1));
        }


        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }


//------------------------------------------------------------------------------

    /**
     * 
     * @param atmType
     * @return
     * @throws KrakenException 
     */
    
    private static double getNuclearCharge(String atmType) throws KrakenException
    {
        if (NCMAP.containsKey(atmType))
        {
            return NCMAP.get(atmType);
        }
        else
        {
            throw new KrakenException("Invalid atom type.");
        }
    }

//------------------------------------------------------------------------------

}
