package krakenx.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * Molecular descriptors obtained by radial basis functions centered on different 
 * interatomic distances. See Hemmer et al., J. Vibrat. Spect., 1999, 19, 151 -164.
 * @author VISHWESH
 */
public class RDF 
{
    /**
     * 
     * @param mol atom container
     * @param beta  smoothing factor which defines the probability distribution 
     *             of the individual distances
     * @param lstWts  weights to be applied according to atomic property
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     */
    
    public static DescriptorContainer calculateRDFDescriptors(IAtomContainer mol, 
            double beta, HashMap<String, Boolean> lstWts, MOPACReader moprd, 
            String prefix)
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    ArrayList<Double> chg = new ArrayList<>();
                    for (int i = 0; i < mol.getAtomCount(); i++)
                    {
                        IAtom atm = mol.getAtom(i);
                        chg.add(atm.getCharge());
                    }
                    calculateRDF(mol, beta, chg, false, "chg", desc, names, prefix);
                }
                
                if (entry.getKey().equals("SPOL"))
                {
                    calculateRDF(mol, beta, moprd.getAtomSelfPolarizabilities(), 
                                    true, "spol", desc, names, prefix);
                }
                
                if (entry.getKey().equals("CDEN"))
                {
                    calculateRDF(mol, beta, moprd.getAtomChargeDensities(),
                                    true, "cden", desc, names, prefix);
                }
                
                if (entry.getKey().equals("NDLOC"))
                {
                    calculateRDF(mol, beta, moprd.getNucleophilicDelocalizabilities(), 
                                    true, "nucdloc", desc, names, prefix);
                }
                
                if (entry.getKey().equals("EDLOC"))
                {
                    calculateRDF(mol, beta, moprd.getElectrophilicDelocalizabilities(), 
                                    true, "elcdloc", desc, names, prefix);
                }
                
                if (entry.getKey().equals("RDLOC"))
                {
                    ArrayList<Double> rd = new ArrayList<>();
                    double f = 0;
                    for (int i=0; i<moprd.getElectrophilicDelocalizabilities().size(); i++)
                    {
                        f = (moprd.getElectrophilicDelocalizabilities().get(i) 
                                + moprd.getNucleophilicDelocalizabilities().get(i));
                        rd.add(f);
                    }
                    calculateRDF(mol, beta, rd, true, "radloc", desc, names, prefix);
                }
            }   
        }
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
//------------------------------------------------------------------------------    
    
    /**
     * Calculate the RDF descriptor for the given set of property weights
     * @param mol atom container
     * @param beta smoothing factor which defines the probability distribution 
     *             of the individual distances
     * @param wts vector of weights corresponding to the atoms
     * @param ignoreHydrogen ignore hydrogen atoms if required
     * @param wtType property based weighting to be applied
     * @param desc vector of descriptors
     * @param names vector of names
     * @param prefix  prefix descriptor names with specified text     
     */
    
    private static void calculateRDF(IAtomContainer mol, double beta, 
            ArrayList<Double> wts, boolean ignoreHydrogen, String wtType, 
            ArrayList<Double> desc, ArrayList<String> names, String prefix)
    {
        double[] R = new double[141];
        double start = 1.0;
        for (int i=0; i<R.length; i++)
            R[i] = start + i*0.10;

        double sum = 0, r_ij = 0;
        double f1 = 0, f2 = 0;
        
        for (int k=0; k<R.length; k++)
        {
            sum = 0;
            int l1 = 0;
            for (int i = 0; i < mol.getAtomCount()-1; i++)
            {
                if (ignoreHydrogen)
                {
                    if (mol.getAtom(i).getSymbol().equals("H"))
                        continue;
                }
                
                f1 = wts.get(l1);
                l1++;
                
                int l2 = 0;
                for (int j = i+1; j < mol.getAtomCount(); j++)
                {
                    if (ignoreHydrogen)
                    {
                        if (mol.getAtom(j).getSymbol().equals("H"))
                            continue;
                    }
                    
                    f2 = wts.get(l2);
                    l2++;
                    
                    r_ij = Utils.distance(mol.getAtom(i).getPoint3d(), mol.getAtom(j).getPoint3d());
                    sum += f1 * f2 * Math.exp(-beta * (R[k]-r_ij) * (R[k]-r_ij));
                }
            }
            desc.add(sum);
            names.add(prefix + wtType + "_" +  String.format("%.3f", R[k]));
        }
    }
    
//------------------------------------------------------------------------------    
    
}
