package krakenx.descriptors;

/**
 * Charge descriptors based on the MOPAC derived or other user defined charges
 * @author VISHWESH
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IBond;


public class Charge
{

//------------------------------------------------------------------------------

    /**
     * Calculate various atomic charge based descriptors
     * @param mol atom container
     * @param moprd MOPACReader instance
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     */

    public static DescriptorContainer 
        getChargeDescriptors(IAtomContainer mol, MOPACReader moprd, String prefix)
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        desc.add(getMaxCharge(mol));
        desc.add(getMinCharge(mol));
        desc.add(getTotalAbsoluteCharge(mol));
        desc.add(getMeanAbsoluteCharge(mol));
        desc.add(getTotalNegativeCharge(mol));
        desc.add(getTotalPositiveCharge(mol));
        desc.add(getTotalSquaredAtomicCharge(mol));
        desc.add(getChargePolarization(mol));
        desc.add(getLocalDipoleIndex(mol));
        desc.add(getDP(mol));
        desc.add(getSPP(mol));
        desc.add(getTEIAtoms(mol));
        desc.add(getTEIBonds(mol));
        desc.add(getECDCI(mol, moprd.getAtomChargeDensities()));
////        desc.add(getMaxAtomCharge(mol, "C"));
////        desc.add(getMaxAtomCharge(mol, "H"));
////        desc.add(getMaxAtomCharge(mol, "N"));
////        desc.add(getMaxAtomCharge(mol, "O"));
////        desc.add(getMaxAtomCharge(mol, "S"));
////        desc.add(getMaxAtomCharge(mol, "B"));
////        desc.add(getMaxAtomCharge(mol, "P"));
////        desc.add(getMinAtomCharge(mol, "C"));
////        desc.add(getMinAtomCharge(mol, "H"));
////        desc.add(getMinAtomCharge(mol, "N"));
////        desc.add(getMinAtomCharge(mol, "O"));
////        desc.add(getMinAtomCharge(mol, "S"));
////        desc.add(getMinAtomCharge(mol, "P"));
////        desc.add(getMinAtomCharge(mol, "B"));

        desc.add(getTotalAtomCharge(mol, "C"));
        desc.add(getTotalAtomCharge(mol, "H"));
        desc.add(getTotalAtomCharge(mol, "N"));
        desc.add(getTotalAtomCharge(mol, "O"));
        desc.add(getTotalAtomCharge(mol, "S"));
        desc.add(getTotalAtomCharge(mol, "P"));
        desc.add(getTotalAtomCharge(mol, "B"));
        desc.add(getTotalAtomCharge(mol, "Cl"));
        desc.add(getTotalAtomCharge(mol, "Br"));
        desc.add(getTotalAtomCharge(mol, "F"));


        names.add(prefix + "MAXPOSCHG");
        names.add(prefix + "MINNEGCHG");
        names.add(prefix + "TOTABSCHG");
        names.add(prefix + "MEANABSCHG");
        names.add(prefix + "TOTNEGCHG");
        names.add(prefix + "TOTPOSCHG");
        names.add(prefix + "TOTSQCHG");
        names.add(prefix + "CHGPOL");
        names.add(prefix + "LOCDIPOLEINDX");
        names.add(prefix + "DP");
        names.add(prefix + "SPP");
        names.add(prefix + "TEIATOMS");
        names.add(prefix + "TEIBONDS");
        names.add(prefix + "ECDCI");
        names.add(prefix + "chgC");
        names.add(prefix + "chgH");
        names.add(prefix + "chgN");
        names.add(prefix + "chgO");
        names.add(prefix + "chgS");
        names.add(prefix + "chgP");
        names.add(prefix + "chgB");
        names.add(prefix + "chgCl");
        names.add(prefix + "chgBr");
        names.add(prefix + "chgF");
        
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    /**
     * Maximum positive charge 
     * @param mol atom container
     * @return 
     */

    private static double getMaxCharge(IAtomContainer mol)
    {
        double f = Double.MIN_VALUE;

        for (IAtom atm:mol.atoms())
        {
            //System.err.println(atm.getCharge());
            if (atm.getCharge() > f)
            {
                f = atm.getCharge();
            }
        }

        return f;
    }
    
//------------------------------------------------------------------------------
    
    private static double getTotalAtomCharge(IAtomContainer mol, String atype)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            if (atm.getSymbol().equals(atype))
            {
                f += atm.getCharge();
            }
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Most negative charge 
     * @param mol atom container
     * @return
     */

    private static double getMinCharge(IAtomContainer mol)
    {
        double f = Double.MAX_VALUE;
        for (IAtom atm:mol.atoms())
        {
            if (atm.getCharge() < f)
            {
                f = atm.getCharge();
            }
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * sum of the absolute value of the charges
     * @param mol atom container
     * @return
     */

    private static double getTotalAbsoluteCharge(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            f += Math.abs(atm.getCharge());            
        }
        return f;
    }
    
//------------------------------------------------------------------------------

    /**
     * mean of the absolute value of the charges
     * @param mol atom container
     * @return
     */

    private static double getMeanAbsoluteCharge(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            f += Math.abs(atm.getCharge());
        }
        return f/(double)mol.getAtomCount();
    }    

//------------------------------------------------------------------------------

    /**
     * sum of all negative charges
     * @param mol atom container
     * @return
     */

    private static double getTotalNegativeCharge(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            if (atm.getCharge() < 0)
                f += atm.getCharge();
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * sum of all positive charges
     * @param mol atom container
     * @return
     */

    private static double getTotalPositiveCharge(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            if (atm.getCharge() > 0)
                f += atm.getCharge();
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * sum of squared charges
     * @param mol atom container
     * @return
     */

    private static double getTotalSquaredAtomicCharge(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            f += atm.getCharge() * atm.getCharge();
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Charge polarization descriptor
     * @param mol atom container
     * @return 
     */
    
    private static double getChargePolarization(IAtomContainer mol)
    {
        double f = 0;
        for (IAtom atm:mol.atoms())
        {
            f += Math.abs(atm.getCharge());
        }
        return (f/((double) mol.getAtomCount()));
    }

//------------------------------------------------------------------------------

    /**
     * A molecular descriptor calculated as the average of the charge differences
     * over all i-j bonded atom pairs
     * @param mol atom container
     * @return LDI
     */
    private static double getLocalDipoleIndex(IAtomContainer mol)
    {
        double f = 0;
        for (IBond bnd:mol.bonds())
        {
            double chg1 = bnd.getAtom(0).getCharge();
            double chg2 = bnd.getAtom(1).getCharge();
            f += Math.abs(chg1 - chg2);
        }

        f /= (double) mol.getBondCount();

        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Second-order submolecular polarity parameter
     * @param mol atom container
     * @return Qmax-Qmin/Rmm^2
     */
    private static double getDP(IAtomContainer mol)
    {
        double f = 0;

        double f1 = Double.MAX_VALUE, f2 = Double.MIN_VALUE;
        IAtom atm1 = null, atm2 = null;
        for (IAtom atm:mol.atoms())
        {
            if ((atm.getCharge() < 0) && (atm.getCharge() < f1))
            {
                f1 = atm.getCharge();
                atm1 = atm;
            }
        }

        for (IAtom atm:mol.atoms())
        {
            if ((atm.getCharge() > 0) && (atm.getCharge() > f2))
            {
                f2 = atm.getCharge();
                atm2 = atm;
            }
        }

        
        if (atm1 != null && atm2 != null)
        {
            double d;
            d = Utils.distance (atm1.getPoint3d(), atm2.getPoint3d());
            f = Math.abs(f2 - f1)/(d * d);
        }

        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Submolecular polarity parameter
     * @param mol atom container
     * @return Qmax - Qmin
     */

    private static double getSPP(IAtomContainer mol)
    {
        return Math.abs(getMaxCharge(mol) - getMinCharge(mol));
    }

//------------------------------------------------------------------------------

    /**
     * Topological electronic index over bonds
     * @param mol atom container
     * @return
     */

    private static double getTEIBonds(IAtomContainer mol)
    {
        double f = 0, chg1 = 0, chg2 = 0, d = 0;
        for (IBond bnd:mol.bonds())
        {
            chg1 = bnd.getAtom(0).getCharge();
            chg2 = bnd.getAtom(1).getCharge();
            d = Utils.distance (bnd.getAtom(0).getPoint3d(), bnd.getAtom(1).getPoint3d());
            f += Math.abs(chg1 - chg2)/(d*d);
        }
        return f;
    }

//------------------------------------------------------------------------------

    /**
     * Topological electronic index over bonds
     * @param mol atom container
     * @return
     */

    private static double getTEIAtoms(IAtomContainer mol)
    {
        double f = 0, chg1, chg2, d;
        for (int i=0; i<mol.getAtomCount()-1; i++)
        {
            chg1 = mol.getAtom(i).getCharge();
            for (int j=i+1; j<mol.getAtomCount(); j++)
            {
                chg2 = mol.getAtom(j).getCharge();
                d = Utils.distance (mol.getAtom(i).getPoint3d(), mol.getAtom(j).getPoint3d());
                f += Math.abs(chg1 - chg2)/(d*d);
            }
        }
        return f;
    }

//------------------------------------------------------------------------------
    
    /**
     * Electronic charge density connectivity index
     * @param mol atom container
     * @return 
     */

    private static double getECDCI(IAtomContainer mol, ArrayList<Double> chgDen)
    {
        double f = 0, x = 0, chg1, chg2;
        int k = 0;
        IAtom atm1, atm2;
        HashMap<IAtom, Double> atm_cd = new HashMap<>();
        
        for (IAtom atm:mol.atoms())
        {
            if (atm.getSymbol().equals("H"))
                continue;
            k = mol.getAtomNumber(atm);
            x = chgDen.get(k);
            List<IAtom> atmLst = mol.getConnectedAtomsList(atm);
            f = 0;
            for (IAtom atmC:atmLst)
            {
                if (atmC.getSymbol().equals("H"))
                {
                    k = mol.getAtomNumber(atmC);
                    f += chgDen.get(k);
                }
            }
            atm_cd.put(atm, x-f);
        }
        
        f = 0;
        for (IBond bnd:mol.bonds())
        {
            atm1 = bnd.getAtom(0);
            atm2 = bnd.getAtom(1);
            if (atm1.getSymbol().equals("H"))
                continue;
            if (atm2.getSymbol().equals("H"))
                continue;
            chg1 = atm_cd.get(atm1);
            chg2 = atm_cd.get(atm2);
            f += 1/Math.sqrt(chg1 * chg2);
        }
        
        return f;
    }

//------------------------------------------------------------------------------

}

