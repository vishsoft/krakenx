package krakenx.descriptors;

/**
 *
 * @author Vishwesh Venkatraman
 */
import java.util.ArrayList;
import java.util.HashMap;
import krakenx.DescriptorContainer;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

public class DIP
{
    private static final String[] elements = {"B", "Br", "C", "Cl", "F", "I", 
                                              "N", "O", "P", "S", "Si", "Se"};
    
    private static final int NBINS = 10;
    
//------------------------------------------------------------------------------
    
    /**
     * Distance interaction profile descriptors
     * @param mol
     * @param prefix
     * @return
     * @throws Exception
     */
    public static DescriptorContainer 
        getDIPDescripors(IAtomContainer mol, String prefix) throws Exception
    {
        double r_ij;
        String atype1, atype2;
        
        HashMap<String, ArrayList<Integer>> histogram = new HashMap<>();
        initialzeHistogram(histogram);
        
        for (int i = 0; i < mol.getAtomCount()-1; i++)
        {
            IAtom atm1 = mol.getAtom(i);
            atype1 = atm1.getSymbol();
            if (atype1.equals("H"))
                continue;
            for (int j = i+1; j < mol.getAtomCount(); j++)
            {
                IAtom atm2 = mol.getAtom(j);
                atype2 = atm2.getSymbol();
                if (atype2.equals("H"))
                    continue;
                
                r_ij = Utils.distance(atm1.getPoint3d(), atm2.getPoint3d());
                
                updateBin(histogram, atype1, atype2, r_ij);
            }
        }
        
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        for(String key: histogram.keySet())
        {
            ArrayList<Integer> freq = histogram.get(key);
            for (int i=0; i<freq.size(); i++)
            {
                names.add(prefix + key + "_D" + String.format("%.2f", (float) (i+1)));
                desc.add((double) freq.get(i));
            }
        }
        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
//------------------------------------------------------------------------------
        
    /**
     * 
     * @param histogram 
     */    
    
    private static void initialzeHistogram(HashMap<String, ArrayList<Integer>> histogram)
    {
        for (int i=0; i<elements.length; i++)
        {
            for (int j=i+1; j<elements.length; j++)
            {
                ArrayList<Integer> freq = new ArrayList<>();
                for (int k=0; k<NBINS; k++)
                    freq.add(0);
                String atmpair = elements[i] + "-" + elements[j];
                histogram.put(atmpair, freq);
            }
        }
        
        for (int i=0; i<elements.length; i++)
        {
            ArrayList<Integer> freq = new ArrayList<>();
            for (int k=0; k<NBINS; k++)
                freq.add(0);
            String atmpair = elements[i] + "-" + elements[i];
            histogram.put(atmpair, freq);
        }
    }
    
//------------------------------------------------------------------------------
    
    /**
     * 
     * @param histogram
     * @param atype1
     * @param atype2
     * @param value 
     */
    
    private static void updateBin(HashMap<String, ArrayList<Integer>> histogram,
                    String atype1, String atype2, double value)
    {
        String atmpair = atype1 + "-" + atype2;
        
        if (atype1.compareTo(atype2) > 0) // succeeds
        {
            atmpair = atype2 + "-" + atype1;
        }
        
        int bin = (int) Math.round(value);
        
        //System.err.println(atmpair + " " + value + " " + bin);
        
        if (histogram.containsKey(atmpair))
        {
            ArrayList<Integer> freq = histogram.get(atmpair);
            
            if (bin > NBINS)
            {
                int n = freq.get(NBINS-1);
                freq.set(NBINS-1, n+1);
            }
            else  
            {
                int n = freq.get(bin-1);
                freq.set(bin-1, n+1);
            }
        }        
    }
    
//------------------------------------------------------------------------------    
}