package krakenx.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.graph.matrix.TopologicalMatrix;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * Molecular descriptors calculated from molecular graph by summing the products 
 * of atom weights of the terminal atoms of all the paths of the considered path 
 * length (the lag).
 * @author VISHWESH
 */
public class Autocorrelation 
{
    
    /**
     * Autocorrelation descriptors weighted based on charge, 
     * charge density, self polarizability, nucleophilic delocalizability, 
     * electrophilic delocalizability and radical delocalizability
     * @param mol atom container
     * @param lstWts weights to be applied according to atomic property
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of autocorrelation descriptors
     */
    
    public static DescriptorContainer getAutocorrelationDescriptors(IAtomContainer mol,
            HashMap<String, Boolean> lstWts, MOPACReader moprd, String prefix)
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        
        int[][] distancematrix = TopologicalMatrix.getMatrix(mol);
        
        
        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    ArrayList<Double> chg = new ArrayList<>();
                    for (int i = 0; i < mol.getAtomCount(); i++)
                    {
                        IAtom atm = mol.getAtom(i);
                        chg.add(atm.getCharge());
                    }
                    calculateAutoCorrelation(mol, chg, false, "chg", distancematrix,
                                        desc, names, prefix);
                }
                
                if (entry.getKey().equals("CDEN"))
                {
                    calculateAutoCorrelation(mol, moprd.getAtomChargeDensities(),
                            true, "cden", distancematrix, desc, names, prefix);
                }

                if (entry.getKey().equals("SPOL"))
                {
                    calculateAutoCorrelation(mol, moprd.getAtomSelfPolarizabilities(),
                            true, "spol", distancematrix, desc, names, prefix);
                }

                if (entry.getKey().equals("NDLOC"))
                {
                    //System.err.println(Arrays.toString(moprd.getNucleophilicDelocalizabilities().toArray()));
                    calculateAutoCorrelation(mol, moprd.getNucleophilicDelocalizabilities(),
                            true, "nucdloc", distancematrix, desc, names, prefix);
                }

                if (entry.getKey().equals("EDLOC"))
                {
                    //System.err.println(Arrays.toString(moprd.getElectrophilicDelocalizabilities().toArray()));
                    calculateAutoCorrelation(mol, moprd.getElectrophilicDelocalizabilities(),
                            true, "elcdloc", distancematrix, desc, names, prefix);
                }

                if (entry.getKey().equals("RDLOC"))
                {
                    ArrayList<Double> rd = new ArrayList<>();
                    double f = 0;
                    for (int i=0; i<moprd.getElectrophilicDelocalizabilities().size(); i++)
                    {
                        f = (moprd.getElectrophilicDelocalizabilities().get(i)
                                + moprd.getNucleophilicDelocalizabilities().get(i));
                        rd.add(f);
                    }
                    calculateAutoCorrelation(mol, rd, true, "radloc", distancematrix,
                                            desc, names, prefix);
                }
            }
        }

        
        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }
    
//------------------------------------------------------------------------------
    
    
    /**
     * Internal function to calculate the descriptors for a given molecule
     * @param mol atom container
     * @param wts atom property weights
     * @param ignoreHydrogen ignore hydrogen atoms if required
     * @param wtType property based weighting to be applied
     * @param distancematrix matrix of 3D distances
     * @param desc vector of descriptors 
     * @param names vector of descriptor names
     */
    
    private static void calculateAutoCorrelation(IAtomContainer mol, ArrayList<Double> wts,
            boolean ignoreHydrogen, String wtType, int[][] distancematrix, 
            ArrayList<Double> desc, ArrayList<String> names, String prefix)
    {
        int natom = mol.getAtomCount();
        double r_ij = 0, f1 = 0, f2 = 0; 

        for (int k = 1; k <= 10; k++)
        {
            double sum = 0;
            int maxkVertexPairs = 0;
            int l1 = 0;

            for (int i = 0; i < natom-1; i++)
            {
                if (ignoreHydrogen)
                {
                    if (mol.getAtom(i).getSymbol().equals("H"))
                        continue;
                }
                f1 = wts.get(l1);
                l1++;
                
                int l2 = 0;
                for (int j = i+1; j < natom; j++)
                {
                    if (ignoreHydrogen)
                    {
                        if (mol.getAtom(j).getSymbol().equals("H"))
                            continue;
                    }
                    
                    //System.err.println("j:" + j);
                    f2 = wts.get(l2);
                    l2++;
                    
                    if (distancematrix[i][j] == k)
                    {
                        r_ij = Utils.distance(mol.getAtom(i).getPoint3d(), 
                                            mol.getAtom(j).getPoint3d());
                        
                        sum += f1 * f2 * r_ij;
                        
                        ++maxkVertexPairs;
                    }
                }
            }
            
            sum = maxkVertexPairs>0 ? sum / maxkVertexPairs : 0;
            desc.add(sum);
            names.add(prefix + k + "_" + wtType);
        }
    }
    
//------------------------------------------------------------------------------    
}
