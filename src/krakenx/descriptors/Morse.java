package krakenx.descriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import krakenx.DescriptorContainer;
import krakenx.MOPACReader;
import krakenx.Utils;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * The 3D Molecule Representation of Structure based on Electron diffraction (MoRSE) 
 * descriptors provide 3D information from the 3D coordinates by using the same 
 * transform as in electron diffraction (which uses it to prepare theoretical 
 * scattering curves).
 * @see Schuur et al., J.Chem.lnf.Comput.Sci., (1996), 36, 334-344; 
 *      Gasteiger et. al., J .Chem Inf .Comput .Sci., (1996), 36,1030-1037;
 *      Schuur, J. & Gasteiger, J., Anal.Chem., (1997), 83, 2398-2405
 * @author VISHWESH
 */

public class Morse
{
    
//------------------------------------------------------------------------------
    
    /**
     * 3D-MoRSE descriptors weighted by charge, charge density, self polarizability, 
     * nucleophilic delocalizability, electrophilic delocalizability and radical 
     * delocalizability
     * @param mol atom container
     * @param lstWts weights to be applied according to atomic property
     * @param moprd MOPACReader object
     * @param prefix prefix descriptor names with specified text
     * @return vector of descriptors
     */
    
    public static DescriptorContainer calculateMORSEDescriptors(IAtomContainer mol,
            HashMap<String, Boolean> lstWts, MOPACReader moprd, String prefix)
    {
        ArrayList<Double> desc = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        Iterator<Map.Entry<String, Boolean>> iterator = lstWts.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, Boolean> entry = iterator.next();
            if (entry.getValue())
            {
                if (entry.getKey().equals("CHARGE"))
                {
                    ArrayList<Double> chg = new ArrayList<>();
                    for (int i = 0; i < mol.getAtomCount(); i++)
                    {
                        IAtom atm = mol.getAtom(i);
                        chg.add(atm.getCharge());
                    }
                    calculateMORSE(mol, chg, false, "chg", desc, names, prefix);
                }

                if (entry.getKey().equals("SPOL"))
                {
                    calculateMORSE(mol, moprd.getAtomSelfPolarizabilities(),
                                    true, "spol", desc, names, prefix);
                }
                
                if (entry.getKey().equals("CDEN"))
                {
                    calculateMORSE(mol, moprd.getAtomChargeDensities(),
                                    true, "cden", desc, names, prefix);
                }

                if (entry.getKey().equals("NDLOC"))
                {
                    calculateMORSE(mol, moprd.getNucleophilicDelocalizabilities(),
                                    true, "nucdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("EDLOC"))
                {
                    calculateMORSE(mol, moprd.getElectrophilicDelocalizabilities(),
                                    true, "elcdloc", desc, names, prefix);
                }

                if (entry.getKey().equals("RDLOC"))
                {
                    ArrayList<Double> rd = new ArrayList<>();
                    double f;
                    for (int i=0; i<moprd.getElectrophilicDelocalizabilities().size(); i++)
                    {
                        f = (moprd.getElectrophilicDelocalizabilities().get(i)
                                + moprd.getNucleophilicDelocalizabilities().get(i));
                        rd.add(f);
                    }
                    calculateMORSE(mol, rd, true, "radloc", desc, names, prefix);
                }
            }
        }

        DescriptorContainer dc = new DescriptorContainer();
        dc.setDescriptorNames(names);
        dc.setDescriptorValues(desc);
        return dc;
    }

//------------------------------------------------------------------------------

    /**
     * Calculate the MoRSe descriptor for the given set of property weights
     * @param mol atom container
     * @param wts vector of weights corresponding to the atoms
     * @param ignoreHydrogen ignore hydrogen atoms if required
     * @param wtType property based weighting to be applied
     * @param desc vector of descriptors
     * @param names vector of names
     * @param prefix  prefix descriptor names with specified text
     */
    
    private static void calculateMORSE(IAtomContainer mol, ArrayList<Double> wts,
            boolean ignoreHydrogen, String wtType, ArrayList<Double> desc,
            ArrayList<String> names, String prefix)
    {
        double[] s = new double[32];
        s[0] = 0.01;
        for (int i=1; i<s.length; i++)
            s[i] = i;

        double sum = 0, r_ij = 0;
        double f1 = 0, f2 = 0;

        for (int k=0; k<s.length; k++)
        {
            sum = 0;
            int l1 = 0;
            for (int i = 0; i < mol.getAtomCount()-1; i++)
            {
                if (ignoreHydrogen)
                {
                    if (mol.getAtom(i).getSymbol().equals("H"))
                        continue;
                }

                f1 = wts.get(l1);
                l1++;
                int l2 = 0;
                for (int j = i+1; j < mol.getAtomCount(); j++)
                {
                    if (ignoreHydrogen)
                    {
                        if (mol.getAtom(j).getSymbol().equals("H"))
                            continue;
                    }

                    f2 = wts.get(l2);
                    l2++;
                    r_ij = Utils.distance(mol.getAtom(i).getPoint3d(), mol.getAtom(j).getPoint3d());
                    sum += f1 * f2 * Math.sin(s[k] * r_ij)/ (s[k] * r_ij);
                }
            }
            desc.add(sum);
            names.add(prefix + wtType + "_" +  String.format("%.3f", s[k]));
        }
    }

//------------------------------------------------------------------------------
}
