package krakenx.descriptors;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.Arrays;

/**
 *
 * @author VISHWESH
 */
public class PCA
{
    Matrix evec;
    Matrix t;
    double[] eval;
    
//------------------------------------------------------------------------------   

    public PCA(double[][] cmat, double[] wt)
    {
        int ncol = 3;
        int nrow = wt.length;

        // make a copy of the coordinate matrix
        double[][] d = new double[nrow][ncol];
        for (int i = 0; i < nrow; i++) 
        {
            for (int j = 0; j < ncol; j++)
                d[i][j] = cmat[i][j];
        }

        // do mean centering
        for (int i = 0; i < ncol; i++)
        {
            double mean = 0.0;
            for (int j = 0; j < nrow; j++)
                mean += d[j][i];
            mean = mean / (double) nrow;
            for (int j = 0; j < nrow; j++)
                d[j][i] = (d[j][i] - mean);
        }

        // get the covariance matrix
        double[][] covmat = new double[ncol][ncol];
        double sumwt = 0.;
        for (int i = 0; i < nrow; i++)
        {
            //System.err.println("WT: " + wt[i]);
            sumwt += wt[i];
        }

        

        for (int i = 0; i < ncol; i++)
        {
            double meanx = 0;
            for (int k = 0; k < nrow; k++)
                meanx += d[k][i];
            meanx = meanx / (double) nrow;
           
            for (int j = 0; j < ncol; j++)
            {
                double meany = 0.0;
                for (int k = 0; k < nrow; k++)
                    meany += d[k][j];
                meany = meany / (double) nrow;

                double sum = 0.;
                for (int k = 0; k < nrow; k++)
                {
                    sum += wt[k] * (d[k][i] - meanx) * (d[k][j] - meany);
                }
                covmat[i][j] = sum / sumwt;
            }
        }

        // get the loadings (ie eigenvector matrix)
        Matrix m = new Matrix(covmat);
        //m.print(9,5);
        
        EigenvalueDecomposition ed = m.eig();
        this.eval = ed.getRealEigenvalues();
        this.evec = ed.getV();
        Matrix x = new Matrix(d);
        this.t = x.times(this.evec);
    }
    
//------------------------------------------------------------------------------    

    public double[] getEigenvalues()
    {
        return (this.eval);
    }
    
//------------------------------------------------------------------------------    

    public double[][] getScores()
    {
        return (t.getArray());
    }
    
//------------------------------------------------------------------------------    
    
    public void cleanup()
    {
        Arrays.fill(eval, 0);
        eval = null;
        t = null;
        evec = null;
    }
    
//------------------------------------------------------------------------------        
}

