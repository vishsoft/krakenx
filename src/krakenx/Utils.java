package krakenx;

/**
 * Small utility functions for string checking, creating directories, 
 * mathematical functions etc. 
 * @author VISHWESH
 */


import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.vecmath.Point3d;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.atomtype.CDKAtomTypeMatcher;
import org.openscience.cdk.atomtype.EStateAtomTypeMatcher;
import org.openscience.cdk.atomtype.SybylAtomTypeMatcher;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomType;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.interfaces.IRingSet;
import org.openscience.cdk.modeling.builder3d.ForceFieldConfigurator;
import org.openscience.cdk.ringsearch.AllRingsFinder;
import org.openscience.cdk.tools.manipulator.AtomTypeManipulator;

public class Utils 
{
    public final static double SQRTINVPI = 1.0/Math.sqrt(Math.PI * 2);
    public final static double INVPI = 1.0/Math.PI;
    

//------------------------------------------------------------------------------

    // returns a padded string with zeroes for the count,
    public static String getPaddedString(int count, int number)
    {
        String formatted = String.format("%0" + count + "d", number);
        return formatted;
    }    
    
//------------------------------------------------------------------------------    

    /**
     * Distance between 2 points in 3D space
     * @param a
     * @param b
     * @return distance
     */
    public static double distance(Point3d a, Point3d b)
    {
        double dx = a.x - b.x, dy = a.y - b.y, dz = a.z - b.z;
        return Math.sqrt(dx*dx + dy*dy + dz*dz);
    }
    
//------------------------------------------------------------------------------    

    /**
     * Calculate logarithm to base 2
     * @param x
     * @return 
     */
    public static double log2(double x)
    {
        return Math.log(x)/Math.log(2);
    }

//------------------------------------------------------------------------------'

    /**
     * Round number to specified decimal places
     * @param value
     * @param places
     * @return rounded floating point value
     */
    public static double round(double value, int places)
    {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }    
    
//------------------------------------------------------------------------------'    

    /**
     * C++ equivalent of a getchar()
     */

    public static void pause()
    {
        System.err.println("Press a key to continue");
        try
        {
            int inchar = System.in.read();
        }
        catch (IOException e)
        {
            System.err.println("Error reading from user");
        }
    }
    
//------------------------------------------------------------------------------    
    
    
    /**
     * Setup atom types for a molecule according to the specified force field
     * @param mol atom container
     * @param fftype the forcefield type (MMFF94,MM2,SYBYL)
     * @return molecule with configured atom types
     */
    public static boolean percieveAtomTypes(IAtomContainer mol, String fftype)
    {
        if (fftype.equalsIgnoreCase("MMFF94") || fftype.equalsIgnoreCase("MM2"))
        {
            try
            {
                IChemObjectBuilder builder = DefaultChemObjectBuilder.getInstance();
                ForceFieldConfigurator ffc = new ForceFieldConfigurator();
                ffc.setForceFieldConfigurator(fftype, builder);
	        ffc.assignAtomTyps(mol);
            }
            catch (CDKException cdke)
            {
                return false;
            }
        }

        if (fftype.equalsIgnoreCase("SYBYL"))
        {
            try
            {
                SybylAtomTypeMatcher matcher = SybylAtomTypeMatcher.getInstance(mol.getBuilder());
                Iterator<IAtom> atoms = mol.atoms().iterator();
                while (atoms.hasNext())
                {
                    IAtom atom = atoms.next();
                    atom.setAtomTypeName(null);
                    IAtomType matched = matcher.findMatchingAtomType(mol, atom);
                    if (matched != null)
                        AtomTypeManipulator.configure(atom, matched);
                }
            }
            catch (CDKException cdke)
            {
                return false;
            }
        }
        
        if (fftype.equalsIgnoreCase("CDK"))
        {
            try
            {
                IChemObjectBuilder builder = DefaultChemObjectBuilder.getInstance();
                CDKAtomTypeMatcher matcher = CDKAtomTypeMatcher.getInstance(builder);

                Iterator<IAtom> atoms = mol.atoms().iterator();
                while (atoms.hasNext())
                {
                    IAtom atom = atoms.next();
                    atom.setAtomTypeName(null);
                    IAtomType matched = matcher.findMatchingAtomType(mol, atom);
                    if (matched != null)
                        AtomTypeManipulator.configure(atom, matched);
                }
            }
            catch (Exception cdke)
            {
                return false;
            }
        }


        if (fftype.equalsIgnoreCase("ESTATE"))
        {
            try
            {
                EStateAtomTypeMatcher matcher = new EStateAtomTypeMatcher();
                matcher.setRingSet(getRings(mol));

                Iterator<IAtom> atoms = mol.atoms().iterator();
                while (atoms.hasNext())
                {
                    IAtom atom = atoms.next();
                    atom.setAtomTypeName(null);
                    IAtomType matched = matcher.findMatchingAtomType(mol, atom);
                    if (matched != null)
                        AtomTypeManipulator.configure(atom, matched);
                }
            }
            catch (Exception cdke)
            {
                return false;
            }
        }

        return true;
    }
    
//------------------------------------------------------------------------------    
    
    /**
     * Check if the given string only contains alphabets
     * @param name
     * @return 
     */
    public static boolean isAlpha(String name) 
    {
        char[] chars = name.toCharArray();

        for (char c : chars) 
        {
            if(!Character.isLetter(c)) 
                return false;
        }

        return true;
    }

//------------------------------------------------------------------------------

    /**
     * Find rings in the molecule if any
     * @param mol atom container
     * @return
     * @throws Exception 
     */
    private static IRingSet getRings(IAtomContainer mol) throws Exception
    {
        IRingSet rs = null;
        AllRingsFinder arf = new AllRingsFinder();
        rs = arf.findAllRings(mol);
        return (rs);
    }

//------------------------------------------------------------------------------

    /**
     * create a directory with specified name and location
     * @param filename
     * @return <code>true</code> if directory is successfully created
     */
    public static boolean createDirectory(String filename) 
    {
        return (new File(filename)).mkdir();
    }

//------------------------------------------------------------------------------

    /**
     * Check for existence of file
     * @param filename
     * @return <code>true</code> if file exists
     */
    public static boolean checkExists(String filename)
    {
        if (filename.length() > 0)
        {
            return (new File(filename)).exists();
        }
        return false;
    }

//------------------------------------------------------------------------------
    
    /**
     * Create the working directory for electronic structure calculations
     * @return name of directory created
     */
    
    public static String createWorkDirectory()
    {
        boolean success = false;
        String curDir = System.getProperty("user.dir");
        String fileSep = System.getProperty("file.separator");
        
        boolean check = new File(curDir).canWrite();
        if (!check)
            return null;
        
        String dataDir = "";
        while (!success)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
            String str = "RUN" + sdf.format(new Date());
            dataDir = curDir + fileSep + str;
            success = createDirectory(dataDir);
        }
        
        return dataDir;
    }
    
//------------------------------------------------------------------------------    
}
