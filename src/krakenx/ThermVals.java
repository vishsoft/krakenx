package krakenx;

/**
 *
 * @author Vishwesh Venkatraman
 */
public class ThermVals
{
    private double temperature; // in Kelvin
    private double vibEnthalpy, vibHeatCap, vibEntropy; // Vibrational terms
    private double rotEnthalpy, rotHeatCap, rotEntropy; // Rotational terms 
    private double intEnthalpy, intHeatCap, intEntropy; // internal
    private double traEnthalpy, traHeatCap, traEntropy; // Translational terms
    private double totEnthalpy, totHeatCap, totEntropy; // total
    private double heatOfFormation; 
    
    
    public ThermVals()
    {
        
    }
    
    public ThermVals(double _temperature)
    {
        this.temperature = _temperature;
        
    }

    public double getTemperature()
    {
        return temperature;
    }

    public void setTemperature(double temperature)
    {
        this.temperature = temperature;
    }

    public double getVibEnthalpy()
    {
        return vibEnthalpy;
    }

    public void setVibEnthalpy(double vibEnthalpy)
    {
        this.vibEnthalpy = vibEnthalpy;
    }

    public double getVibHeatCap()
    {
        return vibHeatCap;
    }

    public void setVibHeatCap(double vibHeatCap)
    {
        this.vibHeatCap = vibHeatCap;
    }

    public double getVibEntropy()
    {
        return vibEntropy;
    }

    public void setVibEntropy(double vibEntropy)
    {
        this.vibEntropy = vibEntropy;
    }

    public double getRotEnthalpy()
    {
        return rotEnthalpy;
    }

    public void setRotEnthalpy(double rotEnthalpy)
    {
        this.rotEnthalpy = rotEnthalpy;
    }

    public double getRotHeatCap()
    {
        return rotHeatCap;
    }

    public void setRotHeatCap(double rotHeatCap)
    {
        this.rotHeatCap = rotHeatCap;
    }

    public double getRotEntropy()
    {
        return rotEntropy;
    }

    public void setRotEntropy(double rotEntropy)
    {
        this.rotEntropy = rotEntropy;
    }

    public double getIntEnthalpy()
    {
        return intEnthalpy;
    }

    public void setIntEnthalpy(double intEnthalpy)
    {
        this.intEnthalpy = intEnthalpy;
    }

    public double getIntHeatCap()
    {
        return intHeatCap;
    }

    public void setIntHeatCap(double intHeatCap)
    {
        this.intHeatCap = intHeatCap;
    }

    public double getIntEntropy()
    {
        return intEntropy;
    }

    public void setIntEntropy(double intEntropy)
    {
        this.intEntropy = intEntropy;
    }

    public double getTraEnthalpy()
    {
        return traEnthalpy;
    }

    public void setTraEnthalpy(double traEnthalpy)
    {
        this.traEnthalpy = traEnthalpy;
    }

    public double getTraHeatCap()
    {
        return traHeatCap;
    }

    public void setTraHeatCap(double traHeatCap)
    {
        this.traHeatCap = traHeatCap;
    }

    public double getTraEntropy()
    {
        return traEntropy;
    }

    public void setTraEntropy(double traEntropy)
    {
        this.traEntropy = traEntropy;
    }

    public double getHeatOfFormation()
    {
        return heatOfFormation;
    }

    public void setHeatOfFormation(double heatOfFormation)
    {
        this.heatOfFormation = heatOfFormation;
    }

    public double getTotEnthalpy()
    {
        return totEnthalpy;
    }

    public void setTotEnthalpy(double _totEnthalpy)
    {
        this.totEnthalpy = _totEnthalpy;
    }

    public double getTotHeatCap()
    {
        return totHeatCap;
    }

    public void setTotHeatCap(double _totHeatCap)
    {
        this.totHeatCap = _totHeatCap;
    }

    public double getTotEntropy()
    {
        return totEntropy;
    }

    public void setTotEntropy(double _totEntropy)
    {
        this.totEntropy = _totEntropy;
    }
}
