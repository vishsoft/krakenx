package krakenx;

/**
 *
 * @author Vishwesh Venkatraman
 */
public class KrakenException extends Exception
{
//------------------------------------------------------------------------------
    /**
     * Creates a new instance of <code>KrakenException</code> without message.
     */
    public KrakenException() 
    {
    }
    
//------------------------------------------------------------------------------    

    /**
     * Constructs a new KrakenException with the given message and the
     * Exception as cause.
     *
     * @param err for the constructed exception
     * @param cause the Throwable that triggered the NazgulException
     */
    public KrakenException(String err, Throwable cause) 
    {
        super(err, cause);
    }

//------------------------------------------------------------------------------        

    /**
     * Constructs an instance of <code>KrakenException</code> with the 
     * specified cause.
     * 
     * @param cause another exception (throwable).
     */
    public KrakenException(Throwable cause) 
    {
        super(cause);
    }    
    
//------------------------------------------------------------------------------    

    /**
     * Constructs a new KrakenException with the given message 
     *
     * @param err for the constructed exception
     */

    public KrakenException(String err)
    {
        super(err);     
    }    
    
//------------------------------------------------------------------------------        
}
