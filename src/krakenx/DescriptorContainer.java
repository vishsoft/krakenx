package krakenx;

import java.util.ArrayList;

/**
 * The data structure stores the vector of descriptors and the corresponding 
 * names
 * @author VISHWESH
 */
public class DescriptorContainer 
{
    private ArrayList<String> dNames;
    private ArrayList<Double> dValues;
    
//------------------------------------------------------------------------------    

    public ArrayList<String> getDescriptorNames() 
    {
        return dNames;
    }
    
//------------------------------------------------------------------------------    

    public void setDescriptorNames(ArrayList<String> dNames) 
    {
        this.dNames = dNames;
    }
    
//------------------------------------------------------------------------------    

    public ArrayList<Double> getDescriptorValues() 
    {
        return dValues;
    }
    
//------------------------------------------------------------------------------    

    public void setDescriptorValues(ArrayList<Double> dValues) 
    {
        this.dValues = dValues;
    }    
    
//------------------------------------------------------------------------------    
    
    public void cleanup()
    {
        this.dNames.clear();
        this.dValues.clear();
    }
    
//------------------------------------------------------------------------------        
}
