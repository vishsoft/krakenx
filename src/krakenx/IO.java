package krakenx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.vecmath.Point3d;
import org.openscience.cdk.ChemFile;
import org.openscience.cdk.ChemObject;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.MDLV2000Reader;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;

/**
 * Input/output utilities
 * @author Vishwesh Venkatraman
 */
public class IO
{
    private static final String KEYWORDS_1 = "PRECISE EF XYZ SUPER ENPART"
                            + " ALLVEC BONDS LARGE STATIC POLAR ESP CYCLES=3000";
    private static final String KEYWORDS_2 = "OLDGEO FORCE THERMO";

    
//------------------------------------------------------------------------------    
    
    /**
     * Reads a file containing multiple molecules (multiple SD format))
     *
     * @param filename the file containing the molecules
     * @return IAtomContainer[] an array of molecules
     * @throws krakenx.KrakenException
     */
    public static IAtomContainer readSingleSDFFile(String filename) throws KrakenException
    {
        MDLV2000Reader mdlreader = null;
        ArrayList<IAtomContainer> lstContainers = new ArrayList<>();

        try
        {
            mdlreader = new MDLV2000Reader(new FileReader(new File(filename)));
            ChemFile chemFile = (ChemFile) mdlreader.read((ChemObject) new ChemFile());
            lstContainers.addAll(
                    ChemFileManipulator.getAllAtomContainers(chemFile));
        }
        catch (CDKException | IOException cdke)
        {
            throw new KrakenException(cdke);
        }
        finally
        {
            try
            {
                if (mdlreader != null)
                {
                    mdlreader.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }

        if (lstContainers.isEmpty())
        {
            throw new KrakenException("No data found in " + filename);
        }

        return lstContainers.get(0);
    }
    
//------------------------------------------------------------------------------    
    
    /**
     * Read in a list containing filenames
     * @param filename
     * @return a list of filenames
     * @throws KrakenException 
     */
    public static ArrayList<String> readList(String filename) throws KrakenException
    {
        BufferedReader br = null;
        String sCurrentLine;
        
        ArrayList<String> lstFiles = new ArrayList<>();

        try
        {
            br = new BufferedReader(new FileReader(filename));
            while ((sCurrentLine = br.readLine()) != null)
            {
                sCurrentLine = sCurrentLine.trim();
                if (sCurrentLine.length() == 0)
                {
                    continue;
                }
                
                lstFiles.add(sCurrentLine);
            }
        }
        catch (FileNotFoundException fnfe)
        {
            throw new KrakenException(fnfe);
        }
        catch (IOException ioe)
        {
            throw new KrakenException(ioe);
        }        
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }
        
        if (lstFiles.isEmpty())
        {
            throw new KrakenException("No data found in " + filename);
        }
        
        return lstFiles;

    }
    
//------------------------------------------------------------------------------    
    
    /**
     * 
     * @param filename
     * @return
     * <code>true</code> if file exists
     */
    public static boolean checkExists(String filename)
    {
        if (filename.length() > 0)
        {
            return (new File(filename)).exists();
        }
        return false;
    }
    
//------------------------------------------------------------------------------        
    
    /**
     * Read in the list of user-defined atom charges. Must correspond to the atom 
     * order
     * @param filename
     * @return
     * @throws KrakenException 
     */
    
    public static ArrayList<Double> readCharges(String filename) throws KrakenException
    {
        BufferedReader br = null;
        String sCurrentLine;
        
        ArrayList<Double> lstChg = new ArrayList<>();

        try
        {
            br = new BufferedReader(new FileReader(filename));
            while ((sCurrentLine = br.readLine()) != null)
            {
                sCurrentLine = sCurrentLine.trim();
                if (sCurrentLine.length() == 0)
                {
                    continue;
                }
                
                lstChg.add(Double.parseDouble(sCurrentLine));
            }
        }
        catch (FileNotFoundException fnfe)
        {
            throw new KrakenException(fnfe);
        }
        catch (IOException ioe)
        {
            throw new KrakenException(ioe);
        }        
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }
        
        if (lstChg.isEmpty())
        {
            throw new KrakenException("No data found in " + filename);
        }
        
        return lstChg;
    }
    
//------------------------------------------------------------------------------
    
    /**
     * Write the calculated descriptors to a file
     * @param filename
     * @param molname
     * @param desc
     * @param header
     * @param append
     * @throws KrakenException 
     */
    public static void writeDescriptors(String filename, String molname,
            ArrayList<DescriptorContainer> desc, boolean header, boolean append) 
            throws KrakenException
    {
        StringBuilder sbHeader = null;
        StringBuilder sbDesc = new StringBuilder(2048);
        
        if (header)
        {
            sbHeader = new StringBuilder(2048);
        }
        
        boolean hasNA = false;
        
        sbDesc.append(molname).append(" ");
        
        for (DescriptorContainer dc:desc)
        {
            if (header)
            {
                for (String str:dc.getDescriptorNames())
                {
                    sbHeader.append(str).append(" ");
                }
            }
            
            for (Double dbl:dc.getDescriptorValues())
            {
                if (Double.isNaN(dbl))
                {
                    sbDesc.append("NA").append(" ");
                    hasNA = true;
                }
                else if (Double.isInfinite(dbl))
                {
                    sbDesc.append("NA").append(" ");
                    hasNA = true;
                }
                else
                    sbDesc.append(String.format("%8.3f", dbl)).append(" ");
            }
        }
        
        
        FileWriter fw = null;
        try
        {
            fw = new FileWriter(new File(filename), append);
            
            if (header)
            {
                if (sbHeader != null)
                {
                    fw.write(sbHeader.toString().trim() + System.getProperty("line.separator"));
                }
            }
            fw.write(sbDesc.toString().trim() + System.getProperty("line.separator"));
            fw.flush();
        }
        catch (IOException ioe)
        {
            throw new KrakenException(ioe);
        }
        finally
        {
            if (sbHeader != null)
                sbHeader.setLength(0);
            sbDesc.setLength(0);
            try
            {
                if (fw != null)
                {
                    fw.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }

        if (hasNA) 
        {
            System.err.println("#---------------------------------------#");
            System.err.println("Warning: some descriptors have NA values.");
            System.err.println("#---------------------------------------#");
        }
    }
    
//------------------------------------------------------------------------------    
    
    /**
     * Write a MOPAC input file
     * @param filename
     * @param mol
     * @param opttype
     * @throws KrakenException 
     */
    
    public static void writeMOPACFile(String filename, IAtomContainer mol, 
                                        String opttype) throws KrakenException
    {
        StringBuilder sb = new StringBuilder(2048);
        
        sb.append(opttype).append(" ").append(KEYWORDS_1);
        double charge = AtomContainerManipulator.getTotalFormalCharge(mol);
        if (charge > 0 || charge < 0)
        {
            String chgtag = " CHARGE=" + charge;
            sb.append(chgtag);
        }
        
        sb.append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));
        
        
        
        for (int i = 0; i < mol.getAtomCount(); i++)
        {
            IAtom atm = mol.getAtom(i);
            
            Point3d pt = atm.getPoint3d();
            String str = String.format("%-3s%8.5f 1 %8.5f 1 %8.5f 1", atm.getSymbol(), pt.x, pt.y, pt.z);
            sb.append(str).append(System.getProperty("line.separator"));
        }
        sb.append(System.getProperty("line.separator"));
        
        sb.append(opttype).append(" ").append(KEYWORDS_2).append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));
        
        FileWriter fw = null;
        try
        {
            fw = new FileWriter(new File(filename), false);
            fw.write(sb.toString() + System.getProperty("line.separator"));
            fw.flush();
        }
        catch (IOException ioe)
        {
            throw new KrakenException(ioe);
        }
        finally
        {
            sb.setLength(0);
            try
            {
                if (fw != null)
                {
                    fw.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }
    }    
    
    
//------------------------------------------------------------------------------        
    
    public static void readMOPACCOSFile(String filename, IAtomContainer mol) throws KrakenException
    {
        BufferedReader br = null;
        String sCurrentLine;
        
        ArrayList<Double> lstArea = new ArrayList<>();
        ArrayList<Double> lstSigma = new ArrayList<>();
        ArrayList<Double> lstPotn = new ArrayList<>();
        
        boolean found = false;
        double area, sigma, potential;
        

        try
        {
            br = new BufferedReader(new FileReader(filename));
            while ((sCurrentLine = br.readLine()) != null)
            {
                sCurrentLine = sCurrentLine.trim();
                if (sCurrentLine.length() == 0)
                {
                    continue;
                }
                if (sCurrentLine.contains("COORDINATES (X, Y, Z)"))
                {
                    found = true;
                    continue;                 
                }
                
                if (found)
                {
                    String[] arr = sCurrentLine.split("\\s+");
                    area = Double.parseDouble(arr[arr.length-3]);
                    sigma = Double.parseDouble(arr[arr.length-2]);
                    potential = Double.parseDouble(arr[arr.length-1]);
                    lstArea.add(area);
                    lstSigma.add(sigma);
                    lstPotn.add(potential);
                }

            }
        }
        catch (FileNotFoundException fnfe)
        {
            throw new KrakenException(fnfe);
        }
        catch (IOException ioe)
        {
            throw new KrakenException(ioe);
        }        
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (IOException ioe)
            {
                throw new KrakenException(ioe);
            }
        }
    }
    
//------------------------------------------------------------------------------            
    
}
