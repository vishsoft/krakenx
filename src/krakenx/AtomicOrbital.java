package krakenx;

import java.util.ArrayList;

/**
 * Data structure to hold the atomic orbital coefficients
 * @author Vishwesh Venkatraman
 */
public class AtomicOrbital
{
    private String aType;
    private String orbType;
    private int atomNum;
    private ArrayList<Double> coeffs;

//------------------------------------------------------------------------------

    /**
     * 
     * @param _aType
     * @param _orbType
     * @param _atomNum
     * @param _coeffs 
     */
    
    public AtomicOrbital(String _aType, String _orbType, int _atomNum,
            ArrayList<Double> _coeffs)
    {
        this.aType = _aType;
        this.orbType = _orbType;
        this.atomNum = _atomNum;
        this.coeffs = _coeffs;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @param _aType
     * @param _orbType
     * @param _atomNum 
     */

    public AtomicOrbital(String _aType, String _orbType, int _atomNum)
    {
        this.aType = _aType;
        this.orbType = _orbType;
        this.atomNum = _atomNum;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @return the atom type
     */
    
    public String getAtomType()
    {
        return aType;
    }

//------------------------------------------------------------------------------

    /**
     * Set atom type
     * @param _aType 
     */
    
    public void setAtomType(String _aType)
    {
        this.aType = _aType;
    }

//------------------------------------------------------------------------------
    
    /**
     * Get orbital type Px, py ...
     * @return orbType
     */

    public String getOrbitalType()
    {
        return orbType;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @param _orbType 
     */
    
    public void setOrbitalType(String _orbType)
    {
        this.orbType = _orbType;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @return 
     */
    
    public int getAtomNum()
    {
        return atomNum;
    }

//------------------------------------------------------------------------------

    /**
     * 
     * @param _atomNum 
     */
    public void setAtomNum(int _atomNum)
    {
        this.atomNum = _atomNum;
    }

//------------------------------------------------------------------------------

    public ArrayList<Double> getCoeffs()
    {
        return coeffs;
    }

//------------------------------------------------------------------------------

    public void setCoeffs(ArrayList<Double> _coeffs)
    {
        this.coeffs = _coeffs;
    }

//------------------------------------------------------------------------------

}
