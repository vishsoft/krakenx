#!/bin/bash

# Building KrakenX
if [ -d ../lib ]; then
    if [ -d lib ]; then
        cp -r ../lib/*.jar lib/
    else
        cp -r ../lib .        
    fi 
fi


find ../src/krakenx/ -name *.java > javafiles.txt
javac -cp lib/cdk-2.0.jar @javafiles.txt -encoding utf-8 -d .

if [ "$?" != "0" ]; then
    rm javafiles.txt
	echo "Failed to create KrakenX.jar."
    exit -1
fi

rm javafiles.txt


echo "Manifest-Version: 1.0" > manifest.mf
echo "Main-Class: krakenx.KrakenX" >> manifest.mf
echo "Class-Path: lib/cdk-2.0.jar" >> manifest.mf
echo >> manifest.mf

jar cvfm KrakenX.jar manifest.mf krakenx 

if [ "$?" = "0" ]; then
    rm -rf manifest.mf KrakenX
else
	echo "Failed to create KrakenX.jar."
    exit -1
fi

echo "--------------------- Done building KrakenX.jar ---------------------"
