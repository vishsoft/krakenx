#!/bin/bash

# for sfile in mols/*.sdf 
# do
#     fname=`basename $sfile .sdf`    
#     mfile="mopdata/"$fname".out"
#     dfile=$fname"_desc.txt"
#     if [ -f $mfile ]; then
#         if [ ! -f $dfile ]; then
#             ./caldesc.sh $sfile $mfile
#         fi
#     else
#         echo $mfile " not found."
#     fi
# done

OUTFILE="ALLDESC.txt"
header=0


for dfile in *_desc.txt
do
    if [ $header -le 0 ]
    then
        deschdr=`cat $dfile | awk '{if(NR==1) print}'`
        echo $deschdr " " > $OUTFILE
        header=$((header+1))
    fi
    cdata=`cat $dfile | awk '{if(NR==2) {print}}'`
    echo $cdata >> $OUTFILE
done
